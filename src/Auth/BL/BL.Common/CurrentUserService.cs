﻿using BL.CommonModels;
using BL.Interfaces.Common;
using Common.Util;
using Microsoft.Extensions.Logging;
using System;
using System.Threading.Tasks;

namespace BL.Common
{
    public class CurrentUserService : ICurrentUserService
    {
        private readonly IUserInfoService _userInfoService;
        private readonly ILogger<CurrentUserService> _logger;
        public UserInfo UserInfo { get; protected set; }

        public CurrentUserService(IUserInfoService userInfoService, ILogger<CurrentUserService> logger)
        {
            _userInfoService = userInfoService;
            _logger = logger;
        }

        public Task<bool> InitUserInfoAsync(string key)
        {
            if (string.IsNullOrWhiteSpace(key))
                throw new ArgumentNullException(nameof(key));
            try
            {
                return LoadUserInfoFromCacheAsync(key);
            }
            catch (Exception ex)
            {
                _logger.LogError(LoggerEvents.GetFromCache, ex, $"Error 'get' from cache with key {key}, type {typeof(UserInfo)}");
                return Task.FromResult(false);
            }
        }

        private async Task<bool> LoadUserInfoFromCacheAsync(string key)
        {
            UserInfo = await _userInfoService.GetAsync(key);
            return UserInfo != null;
        }

        public Task DeleteCurrentUserInfoAsync()
        {
            return DeleteUserInfoFromCacheAsync(UserInfo.AccessToken);
        }

        private Task DeleteUserInfoFromCacheAsync(string key)
        {
            try
            {
                return _userInfoService.DeleteAsync(key);
            }
            catch (Exception ex)
            {
                _logger.LogError(LoggerEvents.DeleteCurrentUserInfo, ex, "Error on delete key from cache");
                throw;
            }
        }
    }
}
