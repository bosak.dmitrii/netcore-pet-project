﻿using BL.Interfaces.Auth;
using BL.Interfaces.Common;
using Common.Util.OptionsModels;
using Microsoft.Extensions.Options;
using System;
using System.Security.Cryptography;

namespace BL.Common
{
    public class TokenService : ITokenService
    {
        private readonly AuthOptions _authOptions;

        public TokenService(IOptions<AuthOptions> authOptions)
        {
            _authOptions = authOptions.Value;
        }

        public RefreshToken GenerateRefreshToken()
        {
            var randomNumber = new byte[32];
            using (var rng = RandomNumberGenerator.Create())
            {
                rng.GetBytes(randomNumber);
                return new RefreshToken
                {
                    Value = Convert.ToBase64String(randomNumber),
                    ExpirationDate = DateTime.UtcNow.AddMinutes(_authOptions.RefreshTokenLifetime)
                };
            }
        }

        public string GenerateInviteToken()
        {
            var randomNumber = new byte[32];
            using (var rng = RandomNumberGenerator.Create())
            {
                rng.GetBytes(randomNumber);
                return Convert.ToBase64String(randomNumber);
            }
        }
    }
}
