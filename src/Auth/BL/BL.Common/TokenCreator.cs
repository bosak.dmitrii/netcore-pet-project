﻿using BL.Interfaces.Auth;
using BL.Interfaces.Common;
using Common.Util.OptionsModels;
using Microsoft.Extensions.Options;
using System;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Security.Cryptography;
using System.Threading.Tasks;
using DL.Entities;
using Microsoft.IdentityModel.Tokens;

namespace BL.Common
{
    public class TokenCreator : ITokenCreator
    {
        private readonly IClaimsHandler _claimsHandler;
        private readonly AuthOptions _authOptions;

        public TokenCreator(IOptions<AuthOptions> authOptions, IClaimsHandler claimsHandler)
        {
            _claimsHandler = claimsHandler;
            _authOptions = authOptions.Value;
        }

        public RefreshToken GenerateRefreshToken()
        {
            var randomNumber = new byte[32];
            using (var rng = RandomNumberGenerator.Create())
            {
                rng.GetBytes(randomNumber);
                return new RefreshToken
                {
                    Value = Convert.ToBase64String(randomNumber),
                    ExpirationDate = DateTime.UtcNow.AddMinutes(_authOptions.RefreshTokenLifetime)
                };
            }
        }

        public async Task<string> GenerateAccessTokenAsync(User user)
        {
            var claimsIdentity = await _claimsHandler.GetClaimsIdentity(user);
            return GetToken(claimsIdentity);
        }

        private string GetToken(ClaimsIdentity claims)
        {
            if (claims == null)
                throw new ArgumentNullException(nameof(claims));

            var now = DateTime.UtcNow;
            var jwt = new JwtSecurityToken(
                issuer: _authOptions.Issuer,
                audience: _authOptions.Audience,
                notBefore: now,
                claims: claims.Claims,
                expires: now.Add(TimeSpan.FromMinutes(_authOptions.AccessTokenLifetime)),
                signingCredentials: new SigningCredentials(_authOptions.GetSymmetricSecurityKey(), SecurityAlgorithms.HmacSha256));

            return new JwtSecurityTokenHandler().WriteToken(jwt);
        }
    }
}
