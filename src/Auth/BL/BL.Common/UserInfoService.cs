﻿using BL.CommonModels;
using BL.Interfaces.Common;
using Common.Util;
using Common.Util.OptionsModels;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using StackExchange.Redis.Extensions.Core.Abstractions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BL.Common
{
    public class UserInfoService : IUserInfoService
    {
        private readonly ILogger<UserInfoService> _logger;
        private readonly AuthOptions _authOptions;
        private readonly IRedisDatabase _cache;

        public UserInfoService(ILogger<UserInfoService> logger, IOptions<AuthOptions> authOptions, IRedisCacheClient redisCacheClient)
        {
            _logger = logger;
            _cache = redisCacheClient.GetDbFromConfiguration();
            _authOptions = authOptions.Value;
        }

        public Task SetAsync(UserInfo userInfo)
        {
            try
            {
                return _cache.AddAsync(ConfigureKey(userInfo), userInfo, TimeSpan.FromMinutes(_authOptions.AccessTokenLifetime));
            }
            catch (Exception ex)
            {
                _logger.LogError(LoggerEvents.CacheUserInfo, ex, $"Error on {nameof(SetAsync)}, userId {userInfo.Id}, accessToken {userInfo.AccessToken}, email {userInfo.Email}");
                throw new Exception("Add user info to cache exception", ex);
            }
        }

        public async Task<UserInfo> GetAsync(string token)
        {
            var key = await FindKey(token);
            return await _cache.GetAsync<UserInfo>(key);
        }

        public async Task DeleteAsync(string token)
        {
            var key = await FindKey(token);
            await _cache.RemoveAsync(key);
        }

        public async Task DeleteAllAsync(long userId)
        {
            var keys = await FindAllKeys(userId.ToString());
            await _cache.RemoveAllAsync(keys);            
        }

        public async Task<bool> ExistsAsync(string token)
        {
            var key = await FindKey(token);
            return await _cache.ExistsAsync(key);
        }
        
        private string ConfigureKey(UserInfo userInfo)
        {
            return $"_{userInfo.AccessToken}_{userInfo.Id}_";
        }

        private async Task<string> FindKey(string cacheKey)
        {
            var result = await _cache.SearchKeysAsync($"*_{cacheKey}_*");
            return result.FirstOrDefault();
        }

        private Task<IEnumerable<string>> FindAllKeys(string cacheKey)
        {
            return _cache.SearchKeysAsync($"*_{cacheKey}_*");
        }
    }
}
