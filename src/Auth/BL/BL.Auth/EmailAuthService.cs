﻿using AutoMapper;
using BL.Interfaces.Auth;
using BL.Interfaces.Common;
using BL.Interfaces.Notifications;
using BL.Models.Auth;
using BL.Util.Exceptions;
using BL.Util.Identity;
using Common.Util.Enums;
using DL.Entities;
using DL.Interfaces.UnitOfWork;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Logging;
using System.Threading.Tasks;

namespace BL.Auth
{
    public class EmailAuthService : AuthService, IEmailAuthService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IUserInfoService _userInfoService;
        private readonly INotificationFactory _notificationFactory;

        public EmailAuthService(IJwtHandler jwtHandler, IMapper mapper,
            IUnitOfWork unitOfWork, IUserInfoService userInfoService, 
            ILogger<AuthService> logger, UserManager<User> userManager, 
            INotificationFactory notificationFactory) :
            base(jwtHandler, mapper, unitOfWork, userInfoService,
                logger, userManager)
        {
            _unitOfWork = unitOfWork;
            _userInfoService = userInfoService;
            _notificationFactory = notificationFactory;
        }

        public async Task<AuthResultModel> LoginAsync(AuthModel model)
        {
            if (model == null)
                throw new WrongDataException(ErrorMessages.SignInRequestModelIsNull);
            User = await LoadUserByEmailOrDefaultAsync(model.Email) ??
                throw new NoDataException(ErrorMessages.ErrorUserWithThisEmailNotFound);
            await ThrowIfIncorrectPasswordAsync(model.Password);
            return await AuthorizeCurrentUserAsync();
        }

        public async Task<AuthResultModel> RegisterAsync(RegisterRequestModel model)
        {
            if (model == null)
                throw new WrongDataException(ErrorMessages.RegisterRequestModelIsNull);

            await ThrowIfEmailIsAlreadyInUseAsync(model.Email);
            await CreateNewUserAsync(model);
            await AddUserRolesAsync(UserRoleType.Client);
            await CreateEmailVerificationNotificationAsync();
            return AuthResultModel.NeedVerification;
        }

        private async Task CreateNewUserAsync(RegisterRequestModel model)
        {
            User = new User
            {
                Email = model.Email,
                FirstName = model.FirstName,
                LastName = model.LastName,
                UserName = model.Email,
                UserStatus = AccountStateType.NeedVerification
            };
            var result = await UserManager.CreateAsync(User, model.Password);
            result.HandleResult();
        }

        private async Task CreateEmailVerificationNotificationAsync()
        {
            var verificationToken =
                await UserManager.GenerateEmailConfirmationTokenAsync(User);
            await _notificationFactory.BuildSignUpEmailVerification(User.Email, verificationToken);
        }

        public async Task<AuthResultModel> VerifyEmailAsync(ConfirmEmailModel model)
        {
            User = await LoadUserByEmailOrDefaultAsync(model.Email) ??
                throw new NoDataException(ErrorMessages.ErrorUserWithThisEmailNotFound);
            if(await UserManager.IsEmailConfirmedAsync(User))
                throw new ConflictDataException(ErrorMessages.EmailAlreadyVerified);
            await ConfirmEmailAsync(model.Token);
            await UpdateUserStatusAsync();
            return await AuthorizeCurrentUserAsync();
        }

        private async Task ConfirmEmailAsync(string token)
        {
            var result = await UserManager.ConfirmEmailAsync(User, token);
            result.HandleResult();
        }

        private async Task UpdateUserStatusAsync()
        {
            User.UserStatus = AccountStateType.Completed;
            await UserManager.UpdateAsync(User);
        }

        public async Task ResendEmailVerifyAsync(ResendVerifyModel model)
        {
            User = await LoadUserByEmailOrDefaultAsync(model.Email) ??
                throw new NoDataException(ErrorMessages.ErrorUserWithThisEmailNotFound);
            if (await UserManager.IsEmailConfirmedAsync(User))
                throw new ConflictDataException(ErrorMessages.EmailAlreadyVerified);
            await CreateEmailVerificationNotificationAsync();
        }

        public async Task ResetPasswordAsync(string email)
        {
            User = await LoadUserByEmailOrDefaultAsync(email);
            if(User == null) return;
            await CreateResetPasswordNotificationAsync();
        }

        private async Task CreateResetPasswordNotificationAsync()
        {
            var token = await UserManager.GeneratePasswordResetTokenAsync(User);
            await _notificationFactory.BuildResetPasswordNotification(User, token);
        }

        public async Task RestorePasswordAsync(SetPasswordModel model)
        {
            User = await LoadUserByEmailOrDefaultAsync(model.Email) ??
                   throw new NoDataException(ErrorMessages.ErrorUserWithThisEmailNotFound);
            var result = await UserManager.ResetPasswordAsync(User, model.Token, model.Password);
            result.HandleResult();
            await DeleteAllUserLoginsAsync();
        }

        private async Task DeleteAllUserLoginsAsync()
        {
            await _userInfoService.DeleteAllAsync(User.Id);
            var userIdentities = await _unitOfWork.UserIdentities.AllByUserIdAsync(User.Id);
            _unitOfWork.UserIdentities.RemoveRange(userIdentities);
            _unitOfWork.Complete();
        }
    }
}
