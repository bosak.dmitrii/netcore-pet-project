﻿using BL.Interfaces.Auth;
using BL.Interfaces.Common;
using Common.Util.OptionsModels;
using DL.Entities;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using System;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace BL.Auth
{
    public class JwtHandler : IJwtHandler
    {
        private readonly ITokenCreator _tokenCreator;
        private readonly TokenValidationParameters _validationParameters;
        private readonly ISecurityTokenValidator _tokenHandler;

        public JwtHandler(IOptions<AuthOptions> authOptions, ITokenCreator tokenCreator, ISecurityTokenValidator securityTokenValidator = null)
        {
            _tokenCreator = tokenCreator;
            _tokenHandler = securityTokenValidator ?? new JwtSecurityTokenHandler();
            _validationParameters = new TokenValidationParameters
            {
                ValidateAudience = true,
                ValidateIssuer = true,
                ValidateIssuerSigningKey = true,
                IssuerSigningKey = authOptions.Value.GetSymmetricSecurityKey(),
                ValidIssuer = authOptions.Value.Issuer,
                ValidAudience = authOptions.Value.Audience
            };
        }

        public async Task<UserTokenModel> GenerateUserTokenModel(User user)
        {
            if (user == null)
                throw new ArgumentNullException(nameof(user));
            var accessToken = await _tokenCreator.GenerateAccessTokenAsync(user);
            var refreshToken = _tokenCreator.GenerateRefreshToken();
            return new UserTokenModel
            {
                AccessToken = accessToken,
                RefreshToken = refreshToken
            };
        }
        
        private ClaimsPrincipal GetTokenClaimsPrincipal(string token)
        {
            var principal = _tokenHandler.ValidateToken(token, _validationParameters, out SecurityToken securityToken);
            if (!(securityToken is JwtSecurityToken jwtSecurityToken) ||
               !jwtSecurityToken.Header.Alg.Equals(SecurityAlgorithms.HmacSha256, StringComparison.InvariantCultureIgnoreCase))
                throw new ArgumentException("Access token not recognized", nameof(token));
            return principal;
        }

        public string GetClaimValueFromJwtToken(string token, string claimName, bool lifetimeCheck = true)
        {
            if (String.IsNullOrWhiteSpace(token))
                throw new ArgumentNullException(nameof(token));
            if (String.IsNullOrWhiteSpace(claimName))
                throw new ArgumentNullException(nameof(claimName));
            _validationParameters.ValidateLifetime = lifetimeCheck;
            var principal = GetTokenClaimsPrincipal(token);
            return principal.Claims.FirstOrDefault(c => c.Type == claimName)?.Value;
        }
    }
}
