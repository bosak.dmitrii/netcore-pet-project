﻿using AutoMapper;
using BL.CommonModels;
using BL.Interfaces.Auth;
using BL.Interfaces.Common;
using BL.Models.Auth;
using BL.Util.Exceptions;
using BL.Util.Identity;
using Common.Util.Enums;
using DL.Entities;
using DL.Interfaces.UnitOfWork;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Logging;
using System;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace BL.Auth
{
    public class AuthService : IAuthService
    {
        protected readonly UserManager<User> UserManager;

        protected User User;

        private readonly IMapper _mapper;
        private readonly IJwtHandler _jwtHandler;
        private readonly IUnitOfWork _unitOfWork;
        private readonly IUserInfoService _userInfoService;
        private readonly ILogger<AuthService> _logger;

        public AuthService(IJwtHandler jwtHandler, IMapper mapper, IUnitOfWork unitOfWork,
            IUserInfoService userInfoService, ILogger<AuthService> logger, UserManager<User> userManager)
        {
            _jwtHandler = jwtHandler;
            _mapper = mapper;
            _unitOfWork = unitOfWork;
            _userInfoService = userInfoService;
            _logger = logger;
            UserManager = userManager;
        }

        protected async Task<AuthResultModel> AuthorizeCurrentUserAsync()
        {
            var userTokenModel = await CreateUserTokenModelAsync(DateTime.UtcNow);
            await RememberUserTokenAsync(userTokenModel.AccessToken, UserRoleType.Client);
            return new AuthResultModel
            {
                User = _mapper.Map<UserModel>(User),
                Token = new AuthTokenModel
                {
                    AccessToken = userTokenModel.AccessToken,
                    RefreshToken = userTokenModel.RefreshToken.Value
                }
            };
        }

        public async Task<AuthResultModel> RefreshAccessTokenAsync(RefreshTokenModel model)
        {
            if (model == null)
                throw new WrongDataException(ErrorMessages.RefreshTokenModelIsNull);
            await InitUserFromAccessTokenWithoutLifetimeCheckAsync(model.AccessToken);
            await ThrowIfRefreshTokenIsIncorrectAsync(model.RefreshToken);

            var userIdentityModel = await GetUserIdentityByRefreshTokenAsync(model.RefreshToken);
            _unitOfWork.UserIdentities.Remove(userIdentityModel);
            _unitOfWork.Complete();
            await CleanUserTokenFromCacheAsync(model.AccessToken);

            return await AuthorizeCurrentUserAsync();
        }

        private Task CleanUserTokenFromCacheAsync(string accessToken)
        {
            return _userInfoService.DeleteAsync(accessToken);
        }

        private Task RememberUserTokenAsync(string accessToken, UserRoleType userRole)
        {
            return _userInfoService.SetAsync(new UserInfo { AccessToken = accessToken, Email = User.Email, Id = User.Id, UserRole = userRole });
        }

        protected async Task ThrowIfEmailIsAlreadyInUseAsync(string email)
        {
            if (await UserManager.FindByEmailAsync(email) != null)
                throw new ConflictDataException(ErrorMessages.ErrorEmailAlreadyInUse);
        }

        protected async Task<User> LoadUserByEmailOrDefaultAsync(string email)
        {
            return await UserManager.FindByEmailAsync(email);
        }

        private async Task<User> LoadUserByIdOrDefaultAsync(long userId)
        {
            return await UserManager.FindByIdAsync(userId.ToString());
        }

        protected async Task ThrowIfIncorrectPasswordAsync(string password)
        {
            if (!await UserManager.CheckPasswordAsync(User, password))
                throw new WrongDataException(ErrorMessages.ErrorIncorrectPassword);
        }

        private async Task InitUserFromAccessTokenWithoutLifetimeCheckAsync(string accessToken)
        {
            try
            {
                var userEmail = _jwtHandler.GetClaimValueFromJwtToken(accessToken, ClaimTypes.Email, false);
                User = await LoadUserByEmailOrDefaultAsync(userEmail) ?? throw new NoDataException(ErrorMessages.ErrorUserWithThisEmailNotFound);
            }
            catch
            {
                throw new WrongDataException(ErrorMessages.ErrorRecognizingAccessToken);
            }
        }

        private async Task ThrowIfRefreshTokenIsIncorrectAsync(string refreshToken)
        {
            var userIdentityModel = await GetUserIdentityByRefreshTokenAsync(refreshToken);
            if (userIdentityModel.ExpirationDate < DateTime.UtcNow)
            {
                throw new WrongDataException(ErrorMessages.ErrorRefreshTokenExpired);
            }
        }

        private async Task<UserIdentity> GetUserIdentityByRefreshTokenAsync(string refreshToken)
        {
            return await _unitOfWork.UserIdentities.FindAsync(
                userIdentity => userIdentity.RefreshToken == refreshToken && userIdentity.UserId == User.Id) ?? 
                   throw new WrongDataException(ErrorMessages.ErrorRefreshTokenNotExists);
        }

        protected async Task AddUserRolesAsync(params UserRoleType[] userRoles)
        {
            var result = await UserManager.AddToRolesAsync(User, userRoles.Select(r => r.ToString()));
            result.HandleResult();
        }

        protected async Task<UserTokenModel> CreateUserTokenModelAsync(DateTime signInDate)
        {
            var userTokenModel = await _jwtHandler.GenerateUserTokenModel(User);
            CreateUserIdentity(userTokenModel.RefreshToken, signInDate);
            return userTokenModel;
        }

        private void CreateUserIdentity(RefreshToken refreshToken, DateTime signInDate)
        {
            _unitOfWork.UserIdentities.Add(new UserIdentity
            {
                RefreshToken = refreshToken.Value,
                ExpirationDate = refreshToken.ExpirationDate,
                UserId = User.Id,
                SignInDateTime = signInDate
            });
            _unitOfWork.Complete();
        }

        public async Task<AuthResultModel> CompleteRegistrationAsync(long userId, CompleteRegistrationModel model)
        {
            User = await LoadUserByIdOrDefaultAsync(userId) ??
                   throw new NoDataException(ErrorMessages.UserWithThisIdNotFound);

            User.FirstName = model.FirstName;
            User.LastName = model.LastName;
            User.UserStatus = AccountStateType.Completed;

            var result = await UserManager.UpdateAsync(User);
            result.HandleResult();

            return await AuthorizeCurrentUserAsync();
        }
    }
}
