﻿using BL.Interfaces.Auth;
using Common.Util;
using Common.Util.Enums;
using DL.Entities;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace BL.Auth
{
    public class ClaimsHandler : IClaimsHandler
    {
        private readonly UserManager<User> _userManager;
        private User _user;

        public ClaimsHandler(UserManager<User> userManager)
        {
            _userManager = userManager;
        }

        public async Task<ClaimsIdentity> GetClaimsIdentity(User user)
        {
            _user = user ?? throw new ArgumentNullException(nameof(user));
            var claims = await GetClaims();
            return new ClaimsIdentity(claims, "Token", ClaimsIdentity.DefaultNameClaimType,
                ClaimsIdentity.DefaultRoleClaimType);
        }

        private async Task<List<Claim>> GetClaims()
        {
            var claims = new List<Claim>
            {
                new Claim(ClaimTypes.Locality, "default"),
                new Claim(Constants.Strings.ApplicationClaimTypes.AccountState, ((AccountStateType)_user.UserStatus).ToString())
            };
            var userRoles = await _userManager.GetRolesAsync(_user);
            claims.AddRange(userRoles.Select(role => new Claim(Constants.Strings.ApplicationClaimTypes.Role, role)));
            return claims;
        }
    }
}
