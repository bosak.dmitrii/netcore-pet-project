﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BL.Models.Auth
{
    public class AuthTokenModel
    {
        public string AccessToken { set; get; }
        public string RefreshToken { set; get; }
    }
}
