﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;
using BL.Util;
using Newtonsoft.Json;

namespace BL.Models.Auth
{
    public class ConfirmEmailModel
    {
        [Required(ErrorMessage = "This field is required")]
        [EmailAddress]
        [JsonConverter(typeof(TrimConverter))]
        public string Email { set; get; }
        [Required(ErrorMessage = "This field is required")]
        [JsonConverter(typeof(TrimConverter))]
        public string Token { set; get; }
    }
}
