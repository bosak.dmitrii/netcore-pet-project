﻿using Common.Util.Enums;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace BL.Models.Auth
{
    public class UserModel
    {
        public int? Id { set; get; }
        [JsonConverter(typeof(StringEnumConverter))]
        public AccountStateType UserState { set; get; } = AccountStateType.NeedVerification;
    }
}
