﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;
using BL.Util;
using Newtonsoft.Json;

namespace BL.Models.Auth
{
    public class RegisterRequestModel : AuthModel
    {
        [Required(ErrorMessage = "This field is required")]
        [MinLength(2, ErrorMessage = "Minimum length is 2")]
        [MaxLength(30, ErrorMessage = "Maximum length is 30")]
        [JsonConverter(typeof(TrimConverter))]
        public string FirstName { set; get; }

        [Required(ErrorMessage = "This field is required")]
        [MinLength(2, ErrorMessage = "Minimum length is 2")]
        [MaxLength(30, ErrorMessage = "Maximum length is 30")]
        [JsonConverter(typeof(TrimConverter))]
        public string LastName { set; get; }
    }
}
