﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;
using BL.Util;
using Newtonsoft.Json;

namespace BL.Models.Auth
{
    public class SetPasswordModel : ConfirmEmailModel
    {
        [Required(ErrorMessage = "This field is required")]
        [MinLength(6, ErrorMessage = "Minimum length is 6")]
        [MaxLength(30, ErrorMessage = "Maximum length is 30")]
        [JsonConverter(typeof(TrimConverter))]
        public string Password { set; get; }
    }
}
