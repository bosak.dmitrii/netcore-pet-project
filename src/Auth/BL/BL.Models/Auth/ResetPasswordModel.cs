﻿using BL.Util;
using Newtonsoft.Json;
using System.ComponentModel.DataAnnotations;

namespace BL.Models.Auth
{
    public class ResetPasswordModel
    {
        [Required(ErrorMessage = "This field is required")]
        [EmailAddress]
        [JsonConverter(typeof(TrimConverter))]
        public string Email { set; get; }
    }
}

