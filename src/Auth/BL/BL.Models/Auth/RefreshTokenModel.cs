﻿using System.ComponentModel.DataAnnotations;

namespace BL.Models.Auth
{
    public class RefreshTokenModel
    {
        [Required]
        public string RefreshToken { set; get; }
        [Required]
        public string AccessToken { set; get; }
    }
}
