﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BL.Models.Auth
{
    public class SignInResultModel : RegisterResultModel
    {
        public AuthTokenModel Token { set; get; }
    }
}
