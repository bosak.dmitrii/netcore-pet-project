﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BL.Models.Auth
{
    public class AuthResultModel
    {
        public UserModel User { set; get; } = new UserModel();
        public AuthTokenModel Token { set; get; }

        public static AuthResultModel NeedVerification =>
            new AuthResultModel();
    }
}
