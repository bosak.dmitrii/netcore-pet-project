﻿using BL.Util;
using Newtonsoft.Json;
using System.ComponentModel.DataAnnotations;

namespace BL.Models.Auth
{
    public class AuthModel
    {
        [Required(ErrorMessage = "This field is required")]
        [EmailAddress]
        [JsonConverter(typeof(TrimConverter))]
        public string Email { set; get; }
        [Required(ErrorMessage = "This field is required")]
        [MinLength(6, ErrorMessage = "Minimum length is 6")]
        [MaxLength(30, ErrorMessage = "Maximum length is 30")]
        [JsonConverter(typeof(TrimConverter))]
        public string Password { set; get; }
    }
}
