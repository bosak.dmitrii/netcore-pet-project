﻿using DL.Entities;

namespace BL.Interfaces.Common.Emails
{
    public interface IEmailSender
    {
        bool SendEmail(Email email);
    }
}
