﻿using System.Threading.Tasks;
using BL.RazorTemplates.Models;

namespace BL.Interfaces.Common.Emails
{
    public interface IEmailBuilder
    {
        Task<string> BuildEmailForEmailVerification(VerifyEmailModel verifyEmailModel);
        Task<string> BuildEmailForResetPassword(ResetPasswordEmailModel resetPasswordEmailModel);
    }
}
