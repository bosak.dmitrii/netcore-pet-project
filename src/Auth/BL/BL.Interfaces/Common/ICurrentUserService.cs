﻿using System.Threading.Tasks;
using BL.CommonModels;

namespace BL.Interfaces.Common
{
    public interface ICurrentUserService
    {
        UserInfo UserInfo { get; }
        Task<bool> InitUserInfoAsync(string key);
        Task DeleteCurrentUserInfoAsync();
    }
}
