﻿using BL.CommonModels;
using System.Threading.Tasks;

namespace BL.Interfaces.Common
{
    public interface IUserInfoService
    {
        Task SetAsync(UserInfo userInfo);
        Task<UserInfo> GetAsync(string token);
        Task DeleteAsync(string token);
        Task DeleteAllAsync(long userId);
        Task<bool> ExistsAsync(string token);
    }
}
