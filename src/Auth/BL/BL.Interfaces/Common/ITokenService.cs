﻿using BL.Interfaces.Auth;

namespace BL.Interfaces.Common
{
    public interface ITokenService
    {
        string GenerateInviteToken();
        RefreshToken GenerateRefreshToken();
    }
}
