﻿using System.Threading.Tasks;
using BL.Interfaces.Auth;
using DL.Entities;

namespace BL.Interfaces.Common
{
    public interface ITokenCreator
    {
        RefreshToken GenerateRefreshToken();
        Task<string> GenerateAccessTokenAsync(User user);
    }
}
