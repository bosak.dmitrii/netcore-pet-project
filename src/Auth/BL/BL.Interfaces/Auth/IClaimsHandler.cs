﻿using DL.Entities;
using System.Security.Claims;
using System.Threading.Tasks;

namespace BL.Interfaces.Auth
{
    public interface IClaimsHandler
    {
        Task<ClaimsIdentity> GetClaimsIdentity(User user);
    }
}
