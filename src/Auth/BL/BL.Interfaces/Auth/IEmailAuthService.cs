﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using BL.Models.Auth;

namespace BL.Interfaces.Auth
{
    public interface IEmailAuthService
    {
        Task<AuthResultModel> RegisterAsync(RegisterRequestModel model);
        Task<AuthResultModel> LoginAsync(AuthModel model);
        Task<AuthResultModel> VerifyEmailAsync(ConfirmEmailModel model);
        Task ResendEmailVerifyAsync(ResendVerifyModel model);
        Task ResetPasswordAsync(string email);
        Task RestorePasswordAsync(SetPasswordModel model);

    }
}
