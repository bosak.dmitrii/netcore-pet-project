﻿using DL.Entities;
using System;
using System.Threading.Tasks;

namespace BL.Interfaces.Auth
{
    public interface IJwtHandler
    {
        Task<UserTokenModel> GenerateUserTokenModel(User user);
        string GetClaimValueFromJwtToken(string token, string claimName, bool lifetimeCheck = true);
    }

    public class RefreshToken
    {
        public string Value { set; get; }
        public DateTime ExpirationDate { set; get; }
    }

    public class UserTokenModel
    {
        public string AccessToken { set; get; }
        public RefreshToken RefreshToken { set; get; }
    }
}
