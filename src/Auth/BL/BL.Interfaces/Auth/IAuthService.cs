﻿using BL.Models.Auth;
using System.Threading.Tasks;

namespace BL.Interfaces.Auth
{
    public interface IAuthService
    {
        Task<AuthResultModel> RefreshAccessTokenAsync(RefreshTokenModel model);
        Task<AuthResultModel> CompleteRegistrationAsync(long userId, CompleteRegistrationModel model);
    }
}
