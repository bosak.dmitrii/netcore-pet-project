﻿using DL.Entities;
using System.Threading.Tasks;

namespace BL.Interfaces.Notifications
{
    public interface INotificationFactory
    {
        Task BuildSignUpEmailVerification(string email, string code);
        Task BuildResetPasswordNotification(User user, string token);
    }
}
