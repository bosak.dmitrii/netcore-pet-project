﻿using System;
using Newtonsoft.Json;

namespace BL.Util
{
    public class TrimConverter : JsonConverter
    {
        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            throw new NotImplementedException();
        }

        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
        {
            return ((string)reader.Value)?.Trim();
        }

        public override bool CanConvert(Type objectType) => objectType == typeof(string);

        public override bool CanWrite => false;
    }
}
