﻿using Microsoft.AspNetCore.Identity;

namespace BL.Util.Identity
{
    public class CustomIdentityErrorDescriber : IdentityErrorDescriber
    {
        public const string ConflictCode = "CONFLICT";
        public const string WrongDataCode = "WRONG_DATA";
        public const string NoDataCode = "NO_DATA";

        public const string Email = nameof(Email);
        public const string Token = nameof(Token);
        public const string UserName = nameof(UserName);
        public const string Password = nameof(Password);

        public override IdentityError ConcurrencyFailure()
        {
            return base.ConcurrencyFailure();
        }

        public override IdentityError DefaultError()
        {
            return base.DefaultError();
        }

        public override IdentityError DuplicateEmail(string email)
        {
            return CustomIdentityError
                .FromBase(base.DuplicateEmail(email))
                .SetCode(ConflictCode)
                .SetField(Email);
        }

        public override IdentityError DuplicateRoleName(string role)
        {
            return base.DuplicateRoleName(role);
        }

        public override IdentityError DuplicateUserName(string userName)
        {
            return CustomIdentityError
                .FromBase(base.DuplicateEmail(userName))
                .SetCode(ConflictCode)
                .SetField(Email);
        }

        public override IdentityError InvalidEmail(string email)
        {
            return CustomIdentityError
                .FromBase(base.InvalidEmail(email))
                .SetCode(WrongDataCode)
                .SetField(Email);
        }

        public override IdentityError InvalidRoleName(string role)
        {
            return base.InvalidRoleName(role);
        }

        public override IdentityError InvalidToken()
        {
            return CustomIdentityError
                .FromBase(base.InvalidToken())
                .SetCode(WrongDataCode)
                .SetField(Token);
        }

        public override IdentityError InvalidUserName(string userName)
        {
            return CustomIdentityError
                .FromBase(base.InvalidEmail(userName))
                .SetCode(WrongDataCode)
                .SetField(Email);
        }

        public override IdentityError LoginAlreadyAssociated()
        {
            return base.LoginAlreadyAssociated();
        }

        public override IdentityError PasswordMismatch()
        {
            return CustomIdentityError
                .FromBase(base.PasswordMismatch())
                .SetCode(WrongDataCode)
                .SetField(Password);
        }

        public override IdentityError PasswordRequiresDigit()
        {
            return CustomIdentityError
                .FromBase(base.PasswordRequiresDigit())
                .SetCode(WrongDataCode)
                .SetField(Password);
        }

        public override IdentityError PasswordRequiresLower()
        {
            return CustomIdentityError
                .FromBase(base.PasswordRequiresLower())
                .SetCode(WrongDataCode)
                .SetField(Password);
        }

        public override IdentityError PasswordRequiresNonAlphanumeric()
        {
            return CustomIdentityError
                .FromBase(base.PasswordRequiresNonAlphanumeric())
                .SetCode(WrongDataCode)
                .SetField(Password);
        }

        public override IdentityError PasswordRequiresUniqueChars(int uniqueChars)
        {
            return CustomIdentityError
                .FromBase(base.PasswordRequiresUniqueChars(uniqueChars))
                .SetCode(WrongDataCode)
                .SetField(Password);
        }

        public override IdentityError PasswordRequiresUpper()
        {
            return CustomIdentityError
                .FromBase(base.PasswordRequiresUpper())
                .SetCode(WrongDataCode)
                .SetField(Password);
        }

        public override IdentityError PasswordTooShort(int length)
        {
            return CustomIdentityError
                .FromBase(base.PasswordTooShort(length))
                .SetCode(WrongDataCode)
                .SetField(Password);
        }

        public override IdentityError RecoveryCodeRedemptionFailed()
        {
            return CustomIdentityError
                .FromBase(base.RecoveryCodeRedemptionFailed())
                .SetCode(WrongDataCode)
                .SetField(Password);
        }

        public override IdentityError UserAlreadyHasPassword()
        {
            return base.UserAlreadyHasPassword();
        }

        public override IdentityError UserAlreadyInRole(string role)
        {
            return base.UserAlreadyInRole(role);
        }

        public override IdentityError UserLockoutNotEnabled()
        {
            return base.UserLockoutNotEnabled();
        }

        public override IdentityError UserNotInRole(string role)
        {
            return base.UserNotInRole(role);
        }
    }

    public static class IdentityErrorExtensions
    {
        public static CustomIdentityError SetField(this CustomIdentityError identityError, string field)
        {
            identityError.Field = field;
            return identityError;
        }

        public static CustomIdentityError SetCode(this CustomIdentityError identityError, string code)
        {
            identityError.Code = code;
            return identityError;
        }
    }

    public class CustomIdentityError : IdentityError
    {
        public string Field { get; set; }

        public static CustomIdentityError FromBase(IdentityError error)
        {
            return new CustomIdentityError
            {
                Code = error.Code,
                Description = error.Description
            };
        }
    }
}
