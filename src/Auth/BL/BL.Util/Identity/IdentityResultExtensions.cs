﻿using System.Linq;
using BL.Util.Exceptions;
using Microsoft.AspNetCore.Identity;

namespace BL.Util.Identity
{
    public static class IdentityResultExtensions
    {
        public static void HandleResult(this IdentityResult result)
        {
            if (result.Succeeded) return;
            var firstError = result.Errors.ToList()[0];
            if (firstError is CustomIdentityError customIdentityError)
                throw ServiceException.FromCustomIdentityError(customIdentityError);
            else
                throw ServiceException.FromIdentityError(firstError);
        }
    }
}
