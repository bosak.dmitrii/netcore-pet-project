﻿namespace BL.Util.Exceptions
{
    public static class ErrorMessages
    {
        public static (string errorCode, string message) SignInRequestModelIsNull =>
            ("SignInModel", "Request model is null");
        public static (string errorCode, string message) RefreshTokenModelIsNull =>
            ("RefreshTokenModel", "Request model is null");
        public static (string errorCode, string message) RegisterRequestModelIsNull =>
            ("RegisterModel", "Request model is null");
        public static (string Name, string Description) ErrorUserWithThisEmailNotFound =>
            ("Email", "User with this email not found.");
        public static (string errorCode, string message) UserWithThisIdNotFound => ("UserId", "User not found");
        public static (string Name, string Description) ErrorIncorrectPassword => ("Password", "Incorrect password");
        public static (string Name, string Description) ErrorRecognizingAccessToken =>
            ("Token", "Cannot recognize access token");
        public static (string Name, string Description) ErrorRefreshTokenExpired =>
            ("RefreshToken", "Refresh token expired");
        public static (string Name, string Description) ErrorRefreshTokenNotExists =>
            ("RefreshToken", "Refresh token not exists");

        public static (string errorCode, string message) ErrorEmailAlreadyInUse =>
            ("EmailError", "Email already in use");

        public static (string errorCode, string message) ErrorVerificationCodeIsInvalid =>
            ($"VerificationCode", $"Verification code is invalid");

        public static (string errorCode, string message) ErrorEmailIsIncorrect =>
            ($"EmailError", $"Email is incorrect");

        public static (string errorCode, string message) EmailAlreadyVerified => ($"ConfirmEmailError", $"Email already confirmed");

    }
}
