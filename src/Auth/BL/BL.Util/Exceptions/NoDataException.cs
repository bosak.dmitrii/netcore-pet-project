﻿namespace BL.Util.Exceptions
{
    public class NoDataException : ServiceException
    {
        public NoDataException(params (string errorCode, string message)[] errors) : base(errors)
        {
        }
    }
}
