﻿namespace BL.Util.Exceptions
{
    public class ConflictDataException : ServiceException
    {
        public ConflictDataException(params (string errorCode, string message)[] errors) : base(errors) { }
    }
}
