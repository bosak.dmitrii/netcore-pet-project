﻿namespace BL.Util.Exceptions
{
    public class NoRightsException : ServiceException
    {
        public NoRightsException(params (string errorCode, string message)[] errors) : base(errors) { }
    }
}
