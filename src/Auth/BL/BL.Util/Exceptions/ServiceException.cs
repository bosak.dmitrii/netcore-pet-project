﻿using BL.Util.Identity;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;

namespace BL.Util.Exceptions
{
    public class ServiceException : Exception
    {
        public Dictionary<string, string> Errors { set; get; } = new Dictionary<string, string>();

        protected ServiceException(string message) : base(message) { }

        protected ServiceException(params (string errorCode, string message)[] errors)
        {
            foreach (var error in errors)
            {
                Errors.Add(error.errorCode, error.message);
            }
        }

        public static ServiceException FromCustomIdentityError(CustomIdentityError error)
        {
            switch (error.Code)
            {
                case CustomIdentityErrorDescriber.WrongDataCode:
                    return new WrongDataException((error.Field, error.Description));
                case CustomIdentityErrorDescriber.NoDataCode:
                    return new NoDataException((error.Field, error.Description));
                case CustomIdentityErrorDescriber.ConflictCode:
                    return new ConflictDataException((error.Field, error.Description));
                default:
                    return new ServiceException((error.Field, error.Code));
            }
        }

        public static Exception FromNonServiceException((string errorCode, string message) error)
        {
            return new Exception($"{error.errorCode}: {error.message}");
        }

        public static ServiceException FromIdentityError(IdentityError error)
        {
            return new ServiceException((error.Code, error.Description));
        }
    }
}
