﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BL.Util.Exceptions
{
    public class WrongDataException : ServiceException
    {
        public WrongDataException(params (string errorCode, string message)[] errors) : base(errors)
        {
        }
    }
}
