﻿using BL.Interfaces.Common.Emails;
using System.Threading.Tasks;
using BL.RazorTemplates;
using BL.RazorTemplates.Models;
using Constants = BL.RazorTemplates.Constants;

namespace BL.Notifications.Email
{
    public class EmailBuilder : IEmailBuilder
    {
        private readonly RazorViewToStringRenderer _razorViewToStringRenderer;

        public EmailBuilder(RazorViewToStringRenderer razorViewToStringRenderer)
        {
            _razorViewToStringRenderer = razorViewToStringRenderer;
        }

        public Task<string> BuildEmailForEmailVerification(VerifyEmailModel verifyEmailModel)
        {
            return _razorViewToStringRenderer.RenderViewToStringAsync(
                Constants.BaseEmailTemplatePath + Constants.SignUpEmailVerificationTemplate, verifyEmailModel);
        }

        public Task<string> BuildEmailForResetPassword(ResetPasswordEmailModel resetPasswordEmailModel)
        {
            return _razorViewToStringRenderer.RenderViewToStringAsync(
                Constants.BaseEmailTemplatePath + Constants.ResetPasswordTemplate, resetPasswordEmailModel);
        }
    }
}
