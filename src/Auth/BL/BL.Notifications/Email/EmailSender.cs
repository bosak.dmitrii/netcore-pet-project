﻿using System;
using MailKit.Net.Smtp;
using BL.Interfaces.Common.Emails;
using Common.Util;
using Common.Util.OptionsModels;
using DL.Entities;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using MimeKit;

namespace BL.Notifications.Email
{
    public class EmailSender : IEmailSender
    {
        private readonly ILogger<EmailSender> _logger;
        private readonly SmtpClientHandler _smtpClientHandler;
        private MimeMessage _message;
        private readonly SmtpOptions _smtpOptions;

        public EmailSender(ILogger<EmailSender> logger, IOptions<SmtpOptions> smtpOptions)
        {
            _logger = logger;
            _smtpClientHandler = new SmtpClientHandler(logger, smtpOptions.Value);
            _smtpOptions = smtpOptions.Value;
        }

        public bool SendEmail(DL.Entities.Email email)
        {
            CreateMessage(email);
            return TrySendToSmtpClient();
        }

        private void CreateMessage(DL.Entities.Email email)
        {
            var fromField = GetFromField(email.SenderUser);

            try
            {
                _message = new MimeMessage();
                _message.From.Add(new MailboxAddress(fromField, _smtpOptions.UserName));
                _message.To.Add(GetTargetMailboxAddress(email));
                _message.Subject = email.Subject;
                if (email.RespondToSender && email.SenderUser != null)
                {
                    _message.ReplyTo.Clear();
                }
                var bodyBuilder = new BodyBuilder { HtmlBody = email.Body };
                _message.Body = bodyBuilder.ToMessageBody();
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Error creating message");
                throw;
            }
        }

        private string GetFromField(User senderUser)
        {
            return senderUser == null ? "TEST_APP" : $"{senderUser.FirstName} {senderUser.LastName} via TEST_APP";
        }

        private MailboxAddress GetTargetMailboxAddress(DL.Entities.Email email)
        {
            var targetName = email.TargetUser?.FirstName ?? email.TargetUserEmail;
            var targetAddress = email.TargetUser?.Email ?? email.TargetUserEmail;
            return new MailboxAddress(targetName, targetAddress);
        }

        private bool TrySendToSmtpClient()
        {
            try
            {
                _smtpClientHandler.SetUp();
                _smtpClientHandler.Send(_message);
                _smtpClientHandler.Dispose();
            }
            catch (Exception ex)
            {
                _logger.LogError(LoggerEvents.SendEmailError, ex, "Error on sending email");
                return false;
            }
            return true;
        }

        private class SmtpClientHandler : IDisposable
        {
            private readonly ILogger _logger;
            private readonly SmtpOptions _smtpOptions;
            private bool _isSetUp;
            private SmtpClient _smtpClient;

            public SmtpClientHandler(ILogger logger, SmtpOptions options)
            {
                _logger = logger;
                _smtpOptions = options;
            }

            public void SetUp()
            {
                _smtpClient = new SmtpClient { ServerCertificateValidationCallback = (s, c, h, e) => true };
                Connect();
                Authenticate();
                _isSetUp = true;
            }

            private void Connect()
            {
                try
                {
                    _smtpClient.Connect(_smtpOptions.Host, _smtpOptions.Port, true);
                }
                catch (Exception ex)
                {
                    _logger.LogError(LoggerEvents.ErrorOnConnectToSmtpClient, ex, $"Error on trying to connect to SMTP client: {_smtpOptions.Host}:{_smtpOptions.Port}");
                    throw;
                }
            }

            private void Authenticate()
            {
                try
                {
                    _smtpClient.Authenticate(_smtpOptions.UserName, _smtpOptions.Password);
                }
                catch (Exception ex)
                {
                    _logger.LogError(LoggerEvents.ErrorAuthenticateSmtpClient, ex, "Error on authenticating SMTP client");
                    throw;
                }
            }

            public void Send(MimeMessage message)
            {
                if (!_isSetUp)
                    throw new InvalidOperationException("you need call SetUp method before sending messages");
                _smtpClient.Send(message);
            }

            public void Dispose()
            {
                if (_smtpClient?.IsConnected ?? false)
                    _smtpClient.Disconnect(true);
                _smtpClient?.Dispose();
                _isSetUp = false;
            }
        }
    }
}
