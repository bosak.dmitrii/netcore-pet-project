﻿using BL.Interfaces.Common.Emails;
using BL.Interfaces.Notifications;
using BL.RazorTemplates.Models;
using DL.Entities;
using DL.Entities.Enums;
using DL.Interfaces.UnitOfWork;
using System.Threading.Tasks;

namespace BL.Notifications
{
    public class NotificationFactory : INotificationFactory
    {
        private readonly IEmailBuilder _emailBuilder;
        private readonly IUnitOfWork _unitOfWork;

        public NotificationFactory(IEmailBuilder emailBuilder, IUnitOfWork unitOfWork)
        {
            _emailBuilder = emailBuilder;
            _unitOfWork = unitOfWork;
        }

        public async Task BuildSignUpEmailVerification(string email, string code)
        {
            var emailBody = await _emailBuilder.BuildEmailForEmailVerification(new VerifyEmailModel
            {
                Code = code,
                UserName = email
            });
            CreateNotificationEmail(email, emailBody, "Please Verify your email");
            _unitOfWork.Complete();
        }

        public async Task BuildResetPasswordNotification(User user, string token)
        {
            var emailBody = await _emailBuilder.BuildEmailForResetPassword(new ResetPasswordEmailModel
            {
                Token = token
            });
            CreateNotificationEmail(user.Id, emailBody, "Reset password");
            _unitOfWork.Complete();
        }

        private void CreateNotificationEmail(long targetUserId, string emailBody, string subject, long? senderId = null, bool respondToSender = false)
        {
            _unitOfWork.Emails.Add(new DL.Entities.Email
            {
                Body = emailBody,
                Status = NotificationStatus.Unsent,
                Subject = subject,
                TargetUserId = targetUserId,
                SenderUserId = senderId,
                RespondToSender = respondToSender
            });
        }

        private void CreateNotificationEmail(string targetUserEmail, string emailBody, string subject, long? senderId = null, bool respondToSender = false)
        {
            _unitOfWork.Emails.Add(new DL.Entities.Email
            {
                Body = emailBody,
                Status = NotificationStatus.Unsent,
                Subject = subject,
                TargetUserEmail = targetUserEmail,
                TargetUserId = null,
                SenderUserId = senderId,
                RespondToSender = respondToSender
            });
        }
    }
}
