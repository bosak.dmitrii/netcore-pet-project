﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BL.CommonModels
{
    public class TempUserModel
    {
        public string Email { set; get; }
        public string PhoneNumber { set; get; }
        public string Code { set; get; }
        public string Password { set; get; }
    }
}
