﻿using System;
using Common.Util.Enums;

namespace BL.CommonModels
{
    public class UserInfo
    {
        public long Id { set; get; }
        public string Email { set; get; }
        public string AccessToken { set; get; }
        public UserRoleType UserRole { set; get; }
    }
}
