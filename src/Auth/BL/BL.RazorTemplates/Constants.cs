﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BL.RazorTemplates
{
    public static class Constants
    {
        public const string BaseEmailTemplatePath = "/Views/Emails/";
        public const string SignUpEmailVerificationTemplate = "SignUpEmailVerification.cshtml";
        public const string ResetPasswordTemplate = "ResetPassword.cshtml";
    }
}
