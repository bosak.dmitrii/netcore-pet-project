﻿namespace BL.RazorTemplates.Models
{
    public class BaseEmailModel
    {
        public string StopReceivingEmailLink { set; get; }
    }
}
