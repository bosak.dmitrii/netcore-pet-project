﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BL.RazorTemplates.Models
{
    public class ResetPasswordEmailModel : BaseEmailModel
    {
        public string Token { set; get; }
    }
}
