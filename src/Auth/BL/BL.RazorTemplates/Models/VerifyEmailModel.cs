﻿namespace BL.RazorTemplates.Models
{
    public class VerifyEmailModel : BaseEmailModel
    {
        public string UserName { set; get; }
        public string Code { set; get; }
    }
}
