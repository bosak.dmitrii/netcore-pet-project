﻿using BL.Interfaces.Common.Emails;
using DL.Entities;
using DL.Entities.Enums;
using DL.Interfaces.UnitOfWork;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace BL.BackgroundTask.BackgroundWorkers
{
    public class EmailWorker : ScopedProcessor
    {
        private IUnitOfWork _unitOfWork;
        private IEmailSender _emailSender;

        public EmailWorker(IServiceScopeFactory serviceScopeFactory, ILogger<EmailWorker> logger) : base(serviceScopeFactory, logger)
        {
        }

        public override Task ProcessInScope(IServiceProvider serviceProvider)
        {
            InitializeServices(serviceProvider);
            var notSentEmails = GetUnsentEmails();
            foreach (var notSentEmail in notSentEmails)
            {
                TrySendEmail(notSentEmail);
            }
            return Task.CompletedTask;
        }

        private void InitializeServices(IServiceProvider serviceProvider)
        {
            _unitOfWork = (IUnitOfWork)serviceProvider.GetService(typeof(IUnitOfWork));
            _emailSender = (IEmailSender)serviceProvider.GetService(typeof(IEmailSender));
        }

        private IEnumerable<Email> GetUnsentEmails()
        {
            if(_unitOfWork.Emails.CountAsync(email=>email.Status == NotificationStatus.Unsent).GetAwaiter().GetResult() == 0)
                return new List<Email>();

            return _unitOfWork.Emails.AllAsync(email => email.Status == NotificationStatus.Unsent,
                email => email.SenderUser, email => email.TargetUser).GetAwaiter().GetResult();
        }

        private void TrySendEmail(Email email)
        {
            if (!_emailSender.SendEmail(email)) return;
            email.Status = NotificationStatus.Sent;
            email.SendDate = DateTime.UtcNow;
            _unitOfWork.Complete();
        }
    }
}
