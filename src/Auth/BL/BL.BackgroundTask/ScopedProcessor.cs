﻿using Common.Util;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using System;
using System.Threading.Tasks;

namespace BL.BackgroundTask
{
    public abstract class ScopedProcessor : BackgroundHostedService
    {
        private readonly IServiceScopeFactory _serviceScopeFactory;
        protected readonly ILogger _logger;

        protected ScopedProcessor(IServiceScopeFactory serviceScopeFactory, ILogger logger)
        {
            _serviceScopeFactory = serviceScopeFactory;
            _logger = logger;
        }

        protected override async Task Process()
        {
            try
            {
                using (var scope = _serviceScopeFactory.CreateScope())
                {
                    await ProcessInScope(scope.ServiceProvider);
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(LoggerEvents.ProcessInScopedProcessorError, ex,
                    "Error while processing background task");
            }

        }

        public abstract Task ProcessInScope(IServiceProvider serviceProvider);
    }
}
