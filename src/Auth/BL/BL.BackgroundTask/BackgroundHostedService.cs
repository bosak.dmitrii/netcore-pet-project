﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Hosting;

namespace BL.BackgroundTask
{
    public abstract class BackgroundHostedService : IHostedService
    {
        private Task _executingTask;
        private readonly CancellationTokenSource _stoppingCancellationTokenSource = new CancellationTokenSource();
        protected TimeSpan Delay = TimeSpan.FromSeconds(10);
        public virtual Task StartAsync(CancellationToken cancellationToken)
        {
            _executingTask = ExecuteAsync(_stoppingCancellationTokenSource.Token);
            if (_executingTask.IsCanceled)
            {
                return _executingTask;
            }

            return Task.CompletedTask;
        }

        public async Task StopAsync(CancellationToken cancellationToken)
        {
            if (_executingTask == null)
                return;
            try
            {
                _stoppingCancellationTokenSource.Cancel();
            }
            finally
            {
                await Task.WhenAny(_executingTask, Task.Delay(Timeout.Infinite, cancellationToken));
            }
        }

        protected virtual async Task ExecuteAsync(CancellationToken cancellationToken)
        {
            do
            {
                await Process();
                await Task.Delay(Delay, cancellationToken);
            } while (!_stoppingCancellationTokenSource.IsCancellationRequested);
        }

        protected abstract Task Process();
    }
}
