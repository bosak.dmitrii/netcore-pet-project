﻿using BL.Models.Auth;
using Common.Util.Enums;
using DL.Entities;

namespace BL.Infrastructure.AutoMapperProfiles
{
    public class AutoMapperDomainProfile : AutoMapper.Profile
    {
        public AutoMapperDomainProfile()
        {
            CreateMap<User, UserModel>()
                .ForMember(userModel => userModel.UserState,
                    options => options.MapFrom(user => user.UserStatus))
                .ForMember(userModel => userModel.Id, options => options.MapFrom(user => user.Id));
        }
    }
}
