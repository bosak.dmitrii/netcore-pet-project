﻿using BL.Auth;
using BL.Common;
using BL.Interfaces.Auth;
using BL.Interfaces.Common;
using BL.Interfaces.Common.Emails;
using BL.Interfaces.Notifications;
using BL.Notifications;
using BL.Notifications.Email;
using BL.RazorTemplates;
using BL.Util.Identity;
using DL.Context;
using DL.Entities;
using DL.Infrastructure;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using StackExchange.Redis.Extensions.Core;
using StackExchange.Redis.Extensions.Core.Abstractions;
using StackExchange.Redis.Extensions.Core.Configuration;
using StackExchange.Redis.Extensions.Core.Implementations;
using StackExchange.Redis.Extensions.Newtonsoft;

namespace BL.Infrastructure
{
    public static class Initializer
    {
        private static IServiceCollection _services;

        public static void AddBusinessServices(this IServiceCollection services, IConfiguration configuration, ILogger logger)
        {
            _services = services;
            logger.LogInformation("Configure business layer services...");
            RegisterServices();
            logger.LogInformation("Setup identity...");
            SetupIdentity();
            logger.LogInformation("Configure identity options...");
            ConfigureIdentityOptions();
            logger.LogInformation("Configure business layer services Done!");
            services.AddDataServices(configuration, logger);
        }

        private static void RegisterServices()
        {
            _services.AddScoped<ICurrentUserService, CurrentUserService>();
            _services.AddScoped<IClaimsHandler, ClaimsHandler>();
            _services.AddScoped<IAuthService, AuthService>();
            _services.AddScoped<IEmailAuthService, EmailAuthService>();
            _services.AddScoped<IUserInfoService, UserInfoService>();
            _services.AddScoped<ITokenCreator, TokenCreator>();
            _services.AddScoped<IJwtHandler, JwtHandler>();
            _services.AddScoped<IEmailSender, EmailSender>();
            _services.AddScoped<IEmailBuilder, EmailBuilder>();
            _services.AddScoped<INotificationFactory, NotificationFactory>();
            _services.AddScoped(typeof(RazorViewToStringRenderer));

            //_services.AddSingleton<IRedisCacheClient, RedisCacheClient>();
            //_services.AddSingleton<IRedisCacheConnectionPoolManager, RedisCacheConnectionPoolManager>();
            //_services.AddSingleton<IRedisDefaultCacheClient, RedisDefaultCacheClient>();
            //_services.AddSingleton<ISerializer, NewtonsoftSerializer>();

        }

        private static void SetupIdentity()
        {
            _services.AddIdentity<User, Role>()
                .AddEntityFrameworkStores<ApplicationDbContext>()
                .AddDefaultTokenProviders()
                .AddErrorDescriber<CustomIdentityErrorDescriber>();
        }

        private static void ConfigureIdentityOptions()
        {
            _services.Configure<IdentityOptions>(options =>
            {
                //password
                options.Password.RequiredLength = 6;
                options.Password.RequireUppercase = false;
                options.Password.RequireDigit = false;
                options.Password.RequireLowercase = false;
                options.Password.RequireNonAlphanumeric = false;

                // signin
                options.SignIn.RequireConfirmedEmail = false;
                options.SignIn.RequireConfirmedPhoneNumber = true;

                //lockout
                options.Lockout.MaxFailedAccessAttempts = 5;

                // user
                options.User.RequireUniqueEmail = true;

            });
        }
    }
}
