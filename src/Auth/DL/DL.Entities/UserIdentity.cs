﻿using DL.Entities.Base;
using System;
using System.Collections.Generic;
using System.Text;

namespace DL.Entities
{
    public class UserIdentity : BaseEntity
    {
        public string RefreshToken { set; get; }
        public DateTime ExpirationDate { set; get; }
        public DateTime SignInDateTime { set; get; }
        public User User { set; get; }
        public long UserId { set; get; }
    }
}
