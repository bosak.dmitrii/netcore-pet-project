﻿using System;

namespace DL.Entities.Base
{
    public interface IBaseEntity
    {
        long Id { set; get; }
        DateTime CreateDate { set; get; }
        DateTime? UpdateDate { set; get; }
    }
}
