﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using Common.Util.Enums;
using DL.Entities.Base;
using Microsoft.AspNetCore.Identity;

namespace DL.Entities
{
    public class User: IdentityUser<long>, IBaseEntity
    {
        public string FirstName { set; get; }
        public string LastName { set; get; }
        public AccountStateType UserStatus { set; get; }
        public DateTime CreateDate { get; set; } = DateTime.UtcNow;
        public DateTime? UpdateDate { get; set; }

        public ICollection<UserIdentity> Identities { protected set; get; } = new List<UserIdentity>();

        public ICollection<Email> InboxEmails { protected set; get; } = new List<Email>();
        public ICollection<Email> SentEmails { protected set; get; } = new List<Email>();
    }
}
