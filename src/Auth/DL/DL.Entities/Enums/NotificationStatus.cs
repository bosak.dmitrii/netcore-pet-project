﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DL.Entities.Enums
{
    public enum NotificationStatus
    {
        Unsent = 0,
        Sent = 1,
    }
}
