﻿using System;
using System.Collections.Generic;
using System.Text;
using DL.Entities.Base;
using DL.Entities.Enums;

namespace DL.Entities
{
    public class Email: BaseEntity
    {
        public long? SenderUserId { set; get; }
        public User SenderUser { set; get; }
        public long? TargetUserId { set; get; }
        public User TargetUser { set; get; }
        public string TargetUserEmail { set; get; }
        public DateTime? SendDate { set; get; }
        public string Subject { set; get; }
        public string Body { set; get; }
        public bool RespondToSender { set; get; }
        public NotificationStatus Status { set; get; }
    }
}
