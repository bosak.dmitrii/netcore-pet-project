﻿using Common.Util;
using DL.Context;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using Microsoft.Extensions.Configuration;
using System;

namespace DL.Migrations
{
    public class ApplicationDbContextFactory : IDesignTimeDbContextFactory<ApplicationDbContext>
    {
        public ApplicationDbContext CreateDbContext(string[] args)
        {
            var configuration = SetupConfigurationRoot();
            var connectionString = configuration.GetConnectionString(Constants.Strings.Database.ConnectionStringName);
            var dbContextOptions = SetupDbContextOptions(connectionString);
            return new ApplicationDbContext(dbContextOptions);
        }

        private IConfigurationRoot SetupConfigurationRoot()
        {
            var isDevelopment = IsDevelopment();
            var builder = SetupConfigurationBuilder(isDevelopment);
            return builder.Build();
        }

        private bool IsDevelopment()
        {
            var devEnvironmentVariable = Environment.GetEnvironmentVariable(Constants.Strings.Environment.EnvironmentConfigPropertyName);
            return string.IsNullOrEmpty(devEnvironmentVariable) || devEnvironmentVariable.ToLower() == Constants.Strings.Environment.DevelopmentEnvironmentValue;
        }

        private IConfigurationBuilder SetupConfigurationBuilder(bool isDevelopment)
        {
            var builder = new ConfigurationBuilder()
                .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true)
                .AddEnvironmentVariables();
            if (isDevelopment)
                builder.AddUserSecrets<ApplicationDbContextFactory>();

            return builder;
        }

        private DbContextOptions<ApplicationDbContext> SetupDbContextOptions(string connectionString)
        {
            var optionsBuilder = new DbContextOptionsBuilder<ApplicationDbContext>();
            optionsBuilder.UseNpgsql(connectionString, options => options.MigrationsAssembly(typeof(ApplicationDbContextFactory).Assembly.GetName().Name));
            return optionsBuilder.Options;
        }
    }
}
