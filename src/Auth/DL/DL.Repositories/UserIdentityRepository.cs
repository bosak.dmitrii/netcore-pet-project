﻿using DL.Entities;
using DL.Interfaces.DbContext;
using DL.Interfaces.Repository;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DL.Repositories
{
    public class UserIdentityRepository : Repository<UserIdentity>, IUserIdentityRepository
    {
        public UserIdentityRepository(IApplicationDbContext dbContext) : base(dbContext)
        {

        }

        public Task<List<UserIdentity>> GetExpiredTokensAsync()
        {
            return Context
                .UserIdentities
                .Where(x => x.ExpirationDate <= DateTime.UtcNow)
                .ToListAsync();
        }

        public DateTime GetLastLoginDateOrDefault(int userId)
        {
            var lastTwoLogins = Context.UserIdentities
                .Where(identity => identity.UserId == userId).OrderByDescending(identity => identity.SignInDateTime).Take(2)
                .ToList();
            return lastTwoLogins.Count == 2
                ? lastTwoLogins[1].SignInDateTime
                : DateTime.MinValue;
        }

        public async Task<List<UserIdentity>> AllByUserIdAsync(long userId)
        {
            return await Context.UserIdentities.Where(identity => identity.UserId == userId).ToListAsync();
        }
    }

}
