﻿
using DL.Entities.Base;
using DL.Interfaces.DbContext;
using DL.Interfaces.Repository;
using DL.Repositories.Helpers;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using DL.Util.Exceptions;

namespace DL.Repositories
{
    public class Repository<TEntity> : IRepository<TEntity> where TEntity : class, IBaseEntity
    {
        public DbSet<TEntity> Set => Context.Set<TEntity>();

        protected readonly IApplicationDbContext Context;

        public Repository(IApplicationDbContext context)
        {
            Context = context;
        }

        public async Task<TEntity> GetByIdAsync(long id)
        {
            return await BuildGetQuery(id).FirstOrDefaultAsync() ?? throw new EntityNotFoundException();
        }

        public Task<TEntity> FindByIdAsync(long id)
        {
            return BuildGetQuery(id).FirstOrDefaultAsync();
        }

        public Task<TEntity> FindAsync(Expression<Func<TEntity, bool>> predicate = null, params Expression<Func<TEntity, object>>[] include)
        {
            return BuildGetQuery(predicate, include).FirstOrDefaultAsync();
        }

        public Task<int> CountAsync(Expression<Func<TEntity, bool>> predicate = null)
        {
            return Set.Where(predicate).CountAsync();
        }

        public async Task<IEnumerable<TEntity>> AllAsync(Expression<Func<TEntity, bool>> predicate = null, params Expression<Func<TEntity, object>>[] include)
        {
            return await Queryable(predicate, include).ToListAsync();
        }

        public async Task<IEnumerable<TEntity>> AllAsync(int offset, int limit, Expression<Func<TEntity, bool>> predicate = null, params Expression<Func<TEntity, object>>[] include)
        {
            return await Queryable(predicate, include).Skip(offset).Take(limit).ToListAsync();
        }


        public virtual void Add(TEntity entity)
        {
            Set.Add(entity);
        }

        public virtual void AddRange(ICollection<TEntity> entities)
        {
            Set.AddRange(entities);
        }

        public void Remove(TEntity entity)
        {
            Set.Attach(entity);
            Set.Remove(entity);
        }

        public void RemoveRange(ICollection<TEntity> entities)
        {
            Set.RemoveRange(entities);
        }

        public void Patch(TEntity entity, string[] properties)
        {
            foreach (var property in properties)
            {
                Context.Entry(entity).Property(property).IsModified = true;
            }
        }


        private IQueryable<TEntity> Queryable(Expression<Func<TEntity, bool>> predicate = null, params Expression<Func<TEntity, object>>[] include)
        {
            return BuildListQuery(predicate, include);
        }


        private IQueryable<TEntity> BuildListQuery(Expression<Func<TEntity, bool>> predicate, Expression<Func<TEntity, object>>[] include)
        {
            IQueryable<TEntity> query = Set;
            include?.ToList().ForEach(x => query = query.Include(x));
            if (predicate != null)
            {
                query = query.Where(predicate).AsQueryable();
            }

            return query;
        }

        private IQueryable<TEntity> BuildGetQuery(long id, Expression<Func<TEntity, bool>> predicate = null,
            Expression<Func<TEntity, object>>[] include = null)
        {
            predicate = predicate == null
                ? x => x.Id == id
                : predicate.Combine(x => x.Id == id);
            return BuildGetQuery(predicate, include);
        }

        private IQueryable<TEntity> BuildGetQuery(Expression<Func<TEntity, bool>> predicate = null, Expression<Func<TEntity, object>>[] include = null)
        {
            IQueryable<TEntity> query = Set;
            include?.ToList().ForEach(x => query = query.Include(x));
            query = predicate != null
                ? query.Where(predicate).AsQueryable()
                : query.Where(x => x.Id == -1);
            return query;
        }
    }
}
