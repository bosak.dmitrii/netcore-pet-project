﻿using DL.Interfaces.DbContext;
using DL.Interfaces.UnitOfWork;
using Microsoft.EntityFrameworkCore.Storage;
using System;
using System.Threading.Tasks;
using DL.Entities;
using DL.Interfaces.Repository;

namespace DL.UnitOfWork
{
    public class UnitOfWork : IUnitOfWork
    {
        public IUserIdentityRepository UserIdentities { get; }
        public IRepository<Email> Emails { get; }
        private readonly IApplicationDbContext _appDbContext;

        public UnitOfWork(IApplicationDbContext appDbContext, IUserIdentityRepository userIdentityRepository, IRepository<Email> emailRepository)
        {
            UserIdentities = userIdentityRepository;
            Emails = emailRepository;
            _appDbContext = appDbContext;
        }

        public virtual int Complete()
        {
            return _appDbContext.SaveChanges();
        }

        public virtual async Task<int> CompleteAsync()
        {
            return await _appDbContext.SaveChangesAsync();
        }

        public IDbContextTransaction BeginTransaction()
        {
            return _appDbContext.Database.BeginTransaction();
        }

        public Guid? CurrentTransaction => _appDbContext.Database.CurrentTransaction?.TransactionId;

        public void Dispose()
        {
            _appDbContext?.Dispose();
        }
    }
}
