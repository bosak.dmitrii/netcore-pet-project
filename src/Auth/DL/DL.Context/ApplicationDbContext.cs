﻿using DL.Entities;
using DL.Entities.Base;
using DL.Interfaces.DbContext;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DL.Context.EntityConfigurations;
using DL.Context.EntityConfigurations.IdentityEntities;

namespace DL.Context
{
    public class ApplicationDbContext : IdentityDbContext<User, Role, long>, IApplicationDbContext
    {

        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options) : base(options)
        {
        }

        public DbSet<UserIdentity> UserIdentities { set; get; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.HasPostgresExtension("uuid-ossp");
            base.OnModelCreating(modelBuilder);
            modelBuilder.ApplyConfiguration(new UserEntityTypeConfiguration());
            modelBuilder.ApplyConfiguration(new UserIdentityEntityTypeConfiguration());
            modelBuilder.ApplyConfiguration(new EmailEntityTypeConfiguration());

            // identity entities
            modelBuilder.ApplyConfiguration(new RoleClaimEntityTypeConfiguration());
            modelBuilder.ApplyConfiguration(new UserClaimEntityTypeConfiguration());
            modelBuilder.ApplyConfiguration(new UserLoginEntityTypeConfiguration());
            modelBuilder.ApplyConfiguration(new UserRoleEntityTypeConfiguration());
            modelBuilder.ApplyConfiguration(new UserTokenEntityTypeConfiguration());
            modelBuilder.ApplyConfiguration(new RoleEntityTypeConfiguration());

        }

        DbSet<T> IDbContext.Set<T>()
        {
            return base.Set<T>();
        }

        EntityEntry<T> IDbContext.Entry<T>(T entity)
        {
            return base.Entry(entity);
        }

        public override int SaveChanges()
        {
            BeforeSave();
            return base.SaveChanges();
        }

        public async Task<int> SaveChangesAsync()
        {
            BeforeSave();
            return await base.SaveChangesAsync();
        }

        private void BeforeSave()
        {
            var entries = GetEntriesWithState(EntityState.Added, EntityState.Modified);
            Parallel.ForEach(entries, entry => entry.ReviseUpdateAndCreateDates());
        }

        private IEnumerable<EntityEntry<IBaseEntity>> GetEntriesWithState(params EntityState[] entityStates)
        {
            return ChangeTracker.Entries<IBaseEntity>().Where(x => entityStates.Any(state => x.State == state));
        }
    }

    public static class EntityEntryExtensions
    {
        public static void ReviseUpdateAndCreateDates(this EntityEntry<IBaseEntity> entityEntry)
        {
            if (entityEntry.State == EntityState.Added)
            {
                entityEntry.Entity.CreateDate = DateTime.UtcNow;
            }
            entityEntry.Entity.UpdateDate = DateTime.UtcNow;
        }
    }
}
