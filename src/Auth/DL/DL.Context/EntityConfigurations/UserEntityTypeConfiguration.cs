﻿using DL.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace DL.Context.EntityConfigurations
{
    public class UserEntityTypeConfiguration : IEntityTypeConfiguration<User>
    {
        public void Configure(EntityTypeBuilder<User> builder)
        {
            builder.Property(user => user.FirstName)
                .HasMaxLength(50);
            builder.Property(user => user.LastName)
                .HasMaxLength(50);
            builder.Property(user => user.UserStatus).HasConversion<short>();
            builder.ToTable("Users");
        }
    }
}
