﻿using System;
using System.Collections.Generic;
using System.Text;
using DL.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace DL.Context.EntityConfigurations
{
    public class UserIdentityEntityTypeConfiguration : IEntityTypeConfiguration<UserIdentity>
    {
        public void Configure(EntityTypeBuilder<UserIdentity> builder)
        {
            builder
                .HasOne(identity => identity.User)
                .WithMany(user => user.Identities)
                .HasForeignKey(identity => identity.UserId);

            builder.Property(i => i.RefreshToken).IsRequired();
            builder.Property(i => i.ExpirationDate).IsRequired();
        }
    }
}
