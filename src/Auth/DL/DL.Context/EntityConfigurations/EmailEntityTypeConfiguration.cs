﻿using System;
using System.Collections.Generic;
using System.Text;
using DL.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace DL.Context.EntityConfigurations
{
    public class EmailEntityTypeConfiguration : IEntityTypeConfiguration<Email>
    {
        public void Configure(EntityTypeBuilder<Email> builder)
        {
            SetupPropertiesAndRelations(builder);
        }

        private void SetupPropertiesAndRelations(EntityTypeBuilder<Email> builder)
        {
            builder.HasOne(email => email.SenderUser)
                .WithMany(user => user.SentEmails)
                .HasForeignKey(email => email.SenderUserId);

            builder.HasOne(email => email.TargetUser)
                .WithMany(user => user.InboxEmails)
                .HasForeignKey(email => email.TargetUserId);
        }
    }
}
