﻿using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace DL.Context.EntityConfigurations.IdentityEntities
{
    public class UserLoginEntityTypeConfiguration : IEntityTypeConfiguration<IdentityUserLogin<long>>
    {
        public void Configure(EntityTypeBuilder<IdentityUserLogin<long>> builder)
        {
            SetupPropertiesAndRelations(builder);
        }

        private void SetupPropertiesAndRelations(EntityTypeBuilder<IdentityUserLogin<long>> builder)
        {
            builder.ToTable("UserLogins");
        }
    }
}
