﻿using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace DL.Context.EntityConfigurations.IdentityEntities
{
    public class UserTokenEntityTypeConfiguration : IEntityTypeConfiguration<IdentityUserToken<long>>
    {
        public void Configure(EntityTypeBuilder<IdentityUserToken<long>> builder)
        {
            SetupPropertiesAndRelations(builder);
        }

        private void SetupPropertiesAndRelations(EntityTypeBuilder<IdentityUserToken<long>> builder)
        {
            builder.ToTable("UserTokens");
        }
    }
}
