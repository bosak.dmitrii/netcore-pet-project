﻿using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace DL.Context.EntityConfigurations.IdentityEntities
{
    public class UserRoleEntityTypeConfiguration : IEntityTypeConfiguration<IdentityUserRole<long>>
    {
        public void Configure(EntityTypeBuilder<IdentityUserRole<long>> builder)
        {
            SetupPropertiesAndRelations(builder);
        }

        private void SetupPropertiesAndRelations(EntityTypeBuilder<IdentityUserRole<long>> builder)
        {
            builder.ToTable("UserRoles");
        }
    }
}
