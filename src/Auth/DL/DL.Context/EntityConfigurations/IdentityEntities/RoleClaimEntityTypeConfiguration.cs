﻿using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace DL.Context.EntityConfigurations.IdentityEntities
{
    public class RoleClaimEntityTypeConfiguration : IEntityTypeConfiguration<IdentityRoleClaim<long>>
    {
        public void Configure(EntityTypeBuilder<IdentityRoleClaim<long>> builder)
        {
            SetupPropertiesAndRelations(builder);
        }

        private void SetupPropertiesAndRelations(EntityTypeBuilder<IdentityRoleClaim<long>> builder)
        {
            builder.ToTable("RoleClaims");
        }
    }
}
