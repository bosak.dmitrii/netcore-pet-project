﻿using System;
using System.Collections.Generic;
using System.Text;
using DL.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace DL.Context.EntityConfigurations
{
    public class RoleEntityTypeConfiguration : IEntityTypeConfiguration<Role>
    {
        private readonly List<Role> _roles;
        public RoleEntityTypeConfiguration()
        {
            _roles = new List<Role>
            {
                new Role
                {
                    Id = 1,
                    Name = "Admin",
                    NormalizedName = "ADMIN",
                    CreateDate = DateTime.UtcNow
                },
                new Role
                {
                    Id = 2,
                    Name = "Client",
                    NormalizedName = "CLIENT",
                    CreateDate = DateTime.UtcNow
                }
            };
        }

        public void Configure(EntityTypeBuilder<Role> builder)
        {
            builder.HasData(_roles);
            builder.ToTable("Roles");
        }
    }
}
