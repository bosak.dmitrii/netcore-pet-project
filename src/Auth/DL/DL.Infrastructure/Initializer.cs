﻿using Common.Util;
using DL.Context;
using DL.Entities;
using DL.Interfaces.DbContext;
using DL.Interfaces.Repository;
using DL.Interfaces.UnitOfWork;
using DL.Migrations;
using DL.Repositories;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;

namespace DL.Infrastructure
{
    public static class Initializer
    {
        private static IServiceCollection _services;

        public static void AddDataServices(this IServiceCollection services, IConfiguration configuration,
            ILogger logger)
        {
            _services = services;
            logger.LogInformation("Configure data layer services...");
            logger.LogInformation("Setup Entity Framework...");
            SetupEntityFramework(configuration);
            logger.LogInformation("Migrate database...");
            MigrateDatabase();
            logger.LogInformation("Register data services...");
            RegisterServices();
            logger.LogInformation("Configure data layer services Done!");

        }

        private static void SetupEntityFramework(IConfiguration configuration)
        {
            var connectionString = configuration.GetConnectionString(Constants.Strings.Database.ConnectionStringName);
            _services.AddEntityFrameworkNpgsql().AddDbContext<ApplicationDbContext>(options =>
            {
                options.UseNpgsql(connectionString,
                    b => b.MigrationsAssembly(typeof(ApplicationDbContextFactory).Assembly.GetName().Name));
            });
        }

        private static void MigrateDatabase()
        {
            var context = (ApplicationDbContext)_services.BuildServiceProvider().GetService(typeof(ApplicationDbContext));
            context.Database.Migrate();
        }

        private static void RegisterServices()
        {
            _services.AddScoped<IUnitOfWork, UnitOfWork.UnitOfWork>();
            _services.AddScoped<IApplicationDbContext, ApplicationDbContext>();
            _services.AddScoped<IRepository<Email>, Repository<Email>>();
            _services.AddScoped<IUserIdentityRepository, UserIdentityRepository>();

        }
    }
}
