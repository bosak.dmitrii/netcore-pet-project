﻿using DL.Entities.Base;
using Microsoft.EntityFrameworkCore.Storage;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using DL.Entities;
using DL.Interfaces.Repository;

namespace DL.Interfaces.UnitOfWork
{
    public interface IUnitOfWork: IDisposable
    {
        IUserIdentityRepository UserIdentities { get; }
        IRepository<Email> Emails { get; }
        int Complete();
        Task<int> CompleteAsync();
        IDbContextTransaction BeginTransaction();
        Guid? CurrentTransaction { get; }
    }
}
