﻿using DL.Entities.Base;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;
using DL.Util.Exceptions;

namespace DL.Interfaces.Repository
{
    public interface IRepository<TEntity> where TEntity : IBaseEntity
    {
        /// <exception cref="EntityNotFoundException"></exception>
        Task<TEntity> GetByIdAsync(long id);
        Task<TEntity> FindByIdAsync(long id);
        Task<TEntity> FindAsync(Expression<Func<TEntity, bool>> predicate = null, params Expression<Func<TEntity, object>>[] include);
        Task<int> CountAsync(Expression<Func<TEntity, bool>> predicate = null);
        Task<IEnumerable<TEntity>> AllAsync(Expression<Func<TEntity, bool>> predicate = null, params Expression<Func<TEntity, object>>[] include);
        Task<IEnumerable<TEntity>> AllAsync(int offset, int limit, Expression<Func<TEntity, bool>> predicate = null, params Expression<Func<TEntity, object>>[] include);
        

        void Add(TEntity entity);
        void AddRange(ICollection<TEntity> entities);
        void Remove(TEntity entity);
        void RemoveRange(ICollection<TEntity> entities);
        void Patch(TEntity entity, string[] properties);
    }
}
