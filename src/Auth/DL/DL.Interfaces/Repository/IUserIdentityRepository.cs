﻿using DL.Entities;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DL.Interfaces.Repository
{
    public interface IUserIdentityRepository : IRepository<UserIdentity>
    {
        Task<List<UserIdentity>> GetExpiredTokensAsync();
        DateTime GetLastLoginDateOrDefault(int userId);
        Task<List<UserIdentity>> AllByUserIdAsync(long userId);
    }
}
