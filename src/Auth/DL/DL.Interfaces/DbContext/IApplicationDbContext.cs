﻿using DL.Entities;
using Microsoft.EntityFrameworkCore;
using System;
using DL.Entities.Base;

namespace DL.Interfaces.DbContext
{
    public interface IApplicationDbContext : IDbContext
    {
        DbSet<User> Users { set; get; }
        DbSet<UserIdentity> UserIdentities { set; get; }
        DbSet<Role> Roles { set; get; }

    }
}
