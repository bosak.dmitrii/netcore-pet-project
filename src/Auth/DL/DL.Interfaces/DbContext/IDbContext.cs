﻿using DL.Entities.Base;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using Microsoft.EntityFrameworkCore.Infrastructure;
using System.Threading.Tasks;

namespace DL.Interfaces.DbContext
{
    public interface IDbContext
    {
        DatabaseFacade Database { get; }
        DbSet<T> Set<T>() where T : class, IBaseEntity;
        EntityEntry<T> Entry<T>(T entity) where T : class, IBaseEntity;
        int SaveChanges();
        Task<int> SaveChangesAsync();
        void Dispose();
    }
}
