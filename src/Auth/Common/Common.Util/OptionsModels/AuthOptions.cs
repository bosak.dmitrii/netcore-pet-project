﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.IdentityModel.Tokens;

namespace Common.Util.OptionsModels
{
    public class AuthOptions
    {
        public string Issuer { set; get; }
        public string Audience { set; get; }
        public string Key { set; get; }
        public int AccessTokenLifetime { set; get; }
        public int RefreshTokenLifetime { set; get; }

        public SymmetricSecurityKey GetSymmetricSecurityKey()
        {
            return new SymmetricSecurityKey(Encoding.ASCII.GetBytes(Key));
        }
    }
}
