﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Common.Util.OptionsModels
{
    public class SmtpOptions
    {
        public string Host { set; get; }
        public int Port { set; get; }
        public string UserName { set; get; }
        public string Password { set; get; }
    }
}
