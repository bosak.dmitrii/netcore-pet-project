﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Common.Util.OptionsModels
{
    public class RedisOptions
    {
        public string Host { set; get; }
        public int Port { set; get; }
        public string KeyPrefix { set; get; }
        public bool Ssl { set; get; }
        public string Password { set; get; }
        public string ConfigString { set; get; }
        public int Database { set; get; }
    }
}
