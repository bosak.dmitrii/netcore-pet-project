﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Common.Util
{
    public static class Constants
    {
        public static class Strings
        {
            public static class Database
            {
                public const string ConnectionStringName = "PostgresqlConnection";
            }

            public static class Environment
            {
                public const string DevelopmentEnvironmentValue = "development";
                public const string EnvironmentConfigPropertyName = "ASPNETCORE_ENVIRONMENT";
            }

            public static class ApplicationClaimTypes
            {
                public const string UserId = "UserId";
                public const string Role = "Role";
                public const string AccountState = "AccountState";
            }

        }
    }
}
