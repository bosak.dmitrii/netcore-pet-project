﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Extensions.Logging;

namespace Common.Util
{
    public static class LoggerEvents
    {
        public static EventId CreateRedisDatabase => new EventId(0, nameof(CreateRedisDatabase));
        public static EventId UnexpectedErrorHandledByMiddleware => new EventId(1, nameof(UnexpectedErrorHandledByMiddleware));
        public static EventId UserApiExceptionHandledByMiddleware => new EventId(2, nameof(UserApiExceptionHandledByMiddleware));
        public static EventId CheckKeyExistsInRedis => new EventId(3, nameof(CheckKeyExistsInRedis));
        public static EventId DeleteCurrentUserInfo => new EventId(4, nameof(DeleteCurrentUserInfo));
        public static EventId CacheUserInfo => new EventId(5, nameof(CacheUserInfo));
        public static EventId GetFromCache => new EventId(6, nameof(GetFromCache));
        public static EventId ExpiredTokenRecognizing => new EventId(7, nameof(ExpiredTokenRecognizing));
        public static EventId GetJwtFromAuthHeader => new EventId(8, nameof(GetJwtFromAuthHeader));
        public static EventId ProcessInScopedProcessorError => new EventId(9, nameof(ProcessInScopedProcessorError));
        public static EventId ErrorOnConnectToSmtpClient => new EventId(10, nameof(ErrorOnConnectToSmtpClient));
        public static EventId SendEmailError => new EventId(11, nameof(SendEmailError));
        public static EventId ErrorAuthenticateSmtpClient => new EventId(12, nameof(ErrorAuthenticateSmtpClient));

    }
}
