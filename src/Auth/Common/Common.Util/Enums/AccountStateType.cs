﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Common.Util.Enums
{
    public enum AccountStateType
    {
        NeedVerification = 10,
        NeedPersonalInfo = 20,
        Completed = 30
    }
}
