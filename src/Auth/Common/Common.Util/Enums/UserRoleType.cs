﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Common.Util.Enums
{
    public enum UserRoleType
    {
        Host = 1,
        Client = 2
    }
}
