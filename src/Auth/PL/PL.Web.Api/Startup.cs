﻿using AutoMapper;
using BL.BackgroundTask.BackgroundWorkers;
using BL.Infrastructure;
using Common.Util.OptionsModels;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Cors.Infrastructure;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http.Features;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ApiExplorer;
using Microsoft.AspNetCore.Mvc.Versioning;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using PL.Util;
using PL.Util.Auth;
using PL.Util.ExcpetionHandler;
using PL.Util.Swagger;
using System;
using System.IO;
using System.Reflection;
using StackExchange.Redis.Extensions.Core.Configuration;
using StackExchange.Redis.Extensions.Newtonsoft;

namespace PL.Web.Api
{
    public class Startup
    {
        private readonly ILogger<Startup> _logger;

        public Startup(IConfiguration configuration, ILogger<Startup> logger)
        {
            _logger = logger;
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }
        
        public void ConfigureServices(IServiceCollection services)
        {
            _logger.LogInformation("Configure services...");
            services.AddVersionedApiExplorer(SetupApiExplorerOptions);
            services.AddMvc(options => options.EnableEndpointRouting = false).SetCompatibilityVersion(CompatibilityVersion.Version_3_0);
            services.AddControllers().AddNewtonsoftJson(SetupJsonOptions);
            services.Configure<FormOptions>(options => options.MultipartBodyLengthLimit = 2_219_715_200);
            services.AddAutoMapper(AppDomain.CurrentDomain.GetAssemblies());
            services.AddCors(SetupCorsOptions);
            ConfigureAuth(services);
            services.AddApiVersioning(SetupApiVersioningOptions);
            var xmlFile = $"{Assembly.GetExecutingAssembly().GetName().Name}.xml";
            var xmlPath = Path.Combine(AppContext.BaseDirectory, xmlFile);
            services.AddSwaggerDocumentation(xmlPath);
            SetupOptions(services);
            services.AddBusinessServices(Configuration, _logger);
            SetupRedis(services);
            services.AddHostedService<EmailWorker>();
        }   

        private void SetupApiExplorerOptions(ApiExplorerOptions options)
        {
            options.GroupNameFormat = "'v'VVV";
            options.SubstituteApiVersionInUrl = true;
        }

        private void SetupJsonOptions(MvcNewtonsoftJsonOptions jsonOptions)
        {      
            jsonOptions.SerializerSettings.DateTimeZoneHandling = DateTimeZoneHandling.Utc;
        }

        private void SetupRedis(IServiceCollection services)
		{
            var redisConfiguration = Configuration.GetSection("Redis").Get<RedisConfiguration>();
            services.AddControllersWithViews();
            services.AddStackExchangeRedisExtensions<NewtonsoftSerializer>(redisConfiguration);
        }

        private void SetupCorsOptions(CorsOptions options)
        {
            options.AddDefaultPolicy(builder =>
                {
                    builder.SetIsOriginAllowed(_ => true)
                        .AllowAnyMethod()
                        .AllowAnyHeader()
                        .AllowCredentials()
                        .WithExposedHeaders("Content-Disposition");
                });
        }

        private void ConfigureAuth(IServiceCollection services)
        {
            _logger.LogInformation("Configure auth...");
            var jwtAuthOptionsSection = Configuration.GetSection("JWT").GetSection("AuthOptions");
            services.Configure<AuthOptions>(jwtAuthOptionsSection);
            var authOptions = new AuthOptions();
            jwtAuthOptionsSection.Bind(authOptions);
            services.AddCustomAuthentication(authOptions);
            services.AddCustomAuthorization();
            services.AddAuthentication();
        }

        private void SetupApiVersioningOptions(ApiVersioningOptions options)
        {
            options.ReportApiVersions = true;
            options.AssumeDefaultVersionWhenUnspecified = true;
            options.DefaultApiVersion = new ApiVersion(1, 0);
        }

        private void SetupOptions(IServiceCollection services)
        {
            var redisOptionsSection = Configuration.GetSection("Redis").GetSection("RedisOptions");
            var redisOptions = new RedisOptions();
            redisOptionsSection.Bind(redisOptions);
            var redisConfiguration = new RedisConfiguration()
            {
                AbortOnConnectFail = true,
                KeyPrefix = redisOptions.KeyPrefix,
                Hosts = new[]
                {
                    new RedisHost() {Host = redisOptions.Host, Port = redisOptions.Port}
                },
                AllowAdmin = true,
                Database = redisOptions.Database,
                Ssl = redisOptions.Ssl,
                Password = redisOptions.Password,
                ServerEnumerationStrategy = new ServerEnumerationStrategy()
                {
                    Mode = ServerEnumerationStrategy.ModeOptions.All,
                    TargetRole = ServerEnumerationStrategy.TargetRoleOptions.Any,
                    UnreachableServerAction = ServerEnumerationStrategy.UnreachableServerActionOptions.Throw
                }
            };
            services.AddSingleton(redisConfiguration);
            services.Configure<RedisOptions>(redisOptionsSection);

            var smtpSection = Configuration.GetSection("Email").GetSection("SmtpClient");
            services.Configure<SmtpOptions>(smtpSection);
        }

        public void Configure(IApplicationBuilder app, IHostingEnvironment env, AutoMapper.IConfigurationProvider autoMapper, IApiVersionDescriptionProvider provider)
        {
            app.UseSwaggerDocumentation(provider);
            app.UseCors();
            _logger.LogInformation($"Environment is {env.EnvironmentName}");

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseHsts();
            }
            app.UseStaticFiles();
            app.UseCookiePolicy();
            app.UseAuthentication();
            app.UseCustomExceptionHandler();
            autoMapper.AssertConfigurationIsValid();
            app.UseHttpsRedirection();
            app.UseMvc();
        }
    }
}
