﻿using BL.Interfaces.Auth;
using BL.Interfaces.Common;
using BL.Models.Auth;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using PL.Util.Auth;
using PL.Util.ExcpetionHandler.Models;
using System.Threading.Tasks;

namespace PL.Web.Api.Controllers
{
    [ApiVersion("1")]
    [Route("api/v{Api-Version:apiVersion}/[controller]")]
    [ApiController]
    public class AuthController : ControllerBase
    {
        private readonly IAuthService _authService;
        private readonly ICurrentUserService _currentUserService;
        private readonly IEmailAuthService _emailAuthService;

        public AuthController(IAuthService authService, ICurrentUserService currentUserService, IEmailAuthService emailAuthService)
        {
            _authService = authService;
            _currentUserService = currentUserService;
            _emailAuthService = emailAuthService;
        }

        /// <summary>
        /// SignIn
        /// </summary>
        /// <remarks>
        /// Sample request:
        ///
        ///     POST
        ///     {
        ///        "Email": "test@test.com",
        ///        "Password": "123456"
        ///     }
        ///
        /// </remarks>
        /// <param name="model">Model with user email and password to SignIn</param>
        /// <response code="200">Signin result model</response>
        /// <response code="400">If request model has validation errors</response>
        /// <response code="404">If user not found</response>
        [HttpPost("email/signin")]
        [ProducesResponseType(typeof(SignInResultModel), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(ExceptionDescriptionModel), StatusCodes.Status400BadRequest)]
        [ProducesResponseType(typeof(ExceptionDescriptionModel), StatusCodes.Status404NotFound)]
        public async Task<IActionResult> Login([FromBody]AuthModel model)
        {
            var result = await _emailAuthService.LoginAsync(model);
            return Ok(result);
        }

        /// <summary>
        /// RegisterAsync user
        /// </summary>
        /// <remarks>
        /// Sample request:
        ///
        ///     POST
        ///     {
        ///        "Email": "test@test.com",
        ///        "Password": "123456",
        ///        "FirstName": "Sam",
        ///        "LastName" : "Smith"
        ///     }
        ///
        /// </remarks>
        /// <returns>Newly created user</returns>
        /// <response code="200">Newly created user</response>
        /// <response code="400">If request model has validation errors</response>
        /// <response code="409">If user already exists</response>
        [HttpPost("email/signup")]
        [ProducesResponseType(typeof(AuthResultModel), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(ExceptionDescriptionModel), StatusCodes.Status400BadRequest)]
        [ProducesResponseType(typeof(ExceptionDescriptionModel), StatusCodes.Status409Conflict)]
        public async Task<IActionResult> Register([FromBody] RegisterRequestModel model)
        {
            var result = await _emailAuthService.RegisterAsync(model);
            return Ok(result);
        }

        /// <summary>
        /// Confirm email
        /// </summary>
        /// <remarks>
        /// Sample request:
        ///
        ///     POST
        ///     {
        ///        "Email": "test@test.com",
        ///        "Token": "kdaoipwjdoiawjhfoiserhjfoiawjefopkaeopdkawpokdopawkd"
        ///     }
        ///
        /// </remarks>
        /// <response code="400">If request model has validation errors</response>
        [HttpPost("email/confirm")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(ExceptionDescriptionModel), StatusCodes.Status400BadRequest)]
        [ProducesResponseType(typeof(ExceptionDescriptionModel), StatusCodes.Status409Conflict)]
        public async Task<IActionResult> VerifyEmail([FromBody] ConfirmEmailModel model)
        {
            var result = await _emailAuthService.VerifyEmailAsync(model);
            return Ok(result);
        }

        /// <summary>
        /// Resend verification email
        /// </summary>
        /// <response code="400">If request model has validation errors</response>
        [HttpPost("email/resend")]
        [ProducesResponseType(StatusCodes.Status202Accepted)]
        [ProducesResponseType(typeof(ExceptionDescriptionModel), StatusCodes.Status400BadRequest)]
        [ProducesResponseType(typeof(ExceptionDescriptionModel), StatusCodes.Status409Conflict)]
        public async Task<IActionResult> ResendVerificationEmail([FromBody] ResendVerifyModel model)
        {
            await _emailAuthService.ResendEmailVerifyAsync(model);
            return Accepted();
        }

        /// <summary>
        /// Reset password
        /// </summary>
        /// <response code="400">If request model has validation errors</response>
        [HttpPost("password/reset")]
        [ProducesResponseType(StatusCodes.Status202Accepted)]
        [ProducesResponseType(typeof(ExceptionDescriptionModel), StatusCodes.Status400BadRequest)]
        [ProducesResponseType(typeof(ExceptionDescriptionModel), StatusCodes.Status409Conflict)]
        public async Task<IActionResult> ResetPassword([FromBody] ResetPasswordModel model)
        {
            await _emailAuthService.ResetPasswordAsync(model.Email);
            return Accepted();
        }

        /// <summary>
        /// Set new password
        /// </summary>
        /// <response code="400">If request model has validation errors</response>
        [HttpPut("password/reset")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(ExceptionDescriptionModel), StatusCodes.Status400BadRequest)]
        [ProducesResponseType(typeof(ExceptionDescriptionModel), StatusCodes.Status409Conflict)]
        public async Task<IActionResult> SetPassword([FromBody] SetPasswordModel model)
        {
            await _emailAuthService.RestorePasswordAsync(model);
            return Ok();
        }

        /// <summary>
        /// Sign out user
        /// </summary>
        /// <response code="200">User successfully signed out</response>
        [HttpPost("signout")]
        [Authorize(AuthenticationSchemes = SchemeNames.CustomBearerScheme, Policy = AuthorizationPolicyTypes.AllowAll)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        public IActionResult SignOut()
        {
            _currentUserService.DeleteCurrentUserInfoAsync();
            return Ok();
        }

        /// <summary>
        /// Refresh the token.
        /// </summary>
        /// <remarks>
        /// Sample request:
        ///
        ///     POST
        ///     {
        ///        "RefreshToken": "YOUR_REFRESH_TOKEN",
        ///        "AccessToken": "TOKEN",
        ///     }
        ///
        /// </remarks>
        /// <param name="model">Model with user's current Access Token and Refresh Token</param>
        /// <response code="400">If refresh token expired</response>
        [HttpPost("token")]
        [ProducesResponseType(typeof(RefreshTokenModel), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(ExceptionDescriptionModel), StatusCodes.Status400BadRequest)]
        public async Task<IActionResult> RefreshToken(RefreshTokenModel model)
        {
            var result = await _authService.RefreshAccessTokenAsync(model);
            return Ok(result);
        }
    }
}
