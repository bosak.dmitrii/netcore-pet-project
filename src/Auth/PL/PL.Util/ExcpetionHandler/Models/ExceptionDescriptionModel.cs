﻿using System;
using System.Collections.Generic;
using System.Text;
using BL.Util.Exceptions;
using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;

namespace PL.Util.ExcpetionHandler.Models
{
    public class ExceptionDescriptionModel
    {
        [JsonIgnore]
        public int StatusCode { set; get; }
        public Dictionary<string, string> ErrorDataDictionary { set; get; }

        private ExceptionDescriptionModel() { }

        public static ExceptionDescriptionModel FromCustomException(ServiceException exception)
        {
            var result = new ExceptionDescriptionModel
            {
                StatusCode = DetectStatusCodeFromServiceException(exception),
                ErrorDataDictionary = exception.Errors
            };
            Exception ex = exception;
            while (ex.InnerException != null)
            {
                result.ErrorDataDictionary.Add(ex.InnerException.Message, ex.InnerException.StackTrace);
                ex = ex.InnerException;
            }

            return result;
        }

        public static ExceptionDescriptionModel FromCommonException(Exception ex)
        {
            var result = new ExceptionDescriptionModel
            {
                StatusCode = 500,
                ErrorDataDictionary = new Dictionary<string, string> { { ex.Message, ex.StackTrace } }
            };
            while (ex.InnerException != null)
            {
                result.ErrorDataDictionary.Add(ex.InnerException.Message, ex.InnerException.StackTrace);
                ex = ex.InnerException;
            }
            return result;
        }

        private static int DetectStatusCodeFromServiceException(ServiceException exception)
        {
            switch (exception)
            {
                case WrongDataException _:
                    return StatusCodes.Status400BadRequest;
                case NoRightsException _:
                    return StatusCodes.Status403Forbidden;
                case NoDataException _:
                    return StatusCodes.Status404NotFound;
                case ConflictDataException _:
                    return StatusCodes.Status409Conflict;
                default:
                    return StatusCodes.Status500InternalServerError;
            }
        }
    }
}
