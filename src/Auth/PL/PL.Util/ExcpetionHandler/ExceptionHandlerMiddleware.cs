﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using BL.Util.Exceptions;
using Common.Util;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using PL.Util.ExcpetionHandler.Models;

namespace PL.Util.ExcpetionHandler
{
    public class ExceptionHandlerMiddleware
    {
        private readonly RequestDelegate _next;
        private HttpContext _httpContext;

        public ExceptionHandlerMiddleware(RequestDelegate next)
        {
            _next = next;
        }

        public async Task Invoke(HttpContext context, ILogger<ExceptionHandlerMiddleware> logger)
        {
            _httpContext = context;
            try
            {
                await _next(context);
            }
            catch (ServiceException ex)
            {
                logger.LogWarning(LoggerEvents.UserApiExceptionHandledByMiddleware, ex, "Exception handled by middleware");
                await WriteExceptionIntoContextResponse(ExceptionDescriptionModel.FromCustomException(ex));
            }
            catch (Exception ex)
            {
                logger.LogError(LoggerEvents.UnexpectedErrorHandledByMiddleware, ex, $"Exception handled by middleware");
                await WriteExceptionIntoContextResponse(ExceptionDescriptionModel.FromCommonException(ex));
            }
        }

        private async Task WriteExceptionIntoContextResponse(ExceptionDescriptionModel exceptionDescription)
        {
            _httpContext.Response.StatusCode = exceptionDescription.StatusCode;
            _httpContext.Response.ContentType = "application/json";
            await _httpContext.Response.WriteAsync(JsonConvert.SerializeObject(exceptionDescription.ErrorDataDictionary));
        }
    }
}
