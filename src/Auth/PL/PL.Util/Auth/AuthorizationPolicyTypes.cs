﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PL.Util.Auth
{
    public static class AuthorizationPolicyTypes
    {
        public const string AllowAll = nameof(AllowAll);
        public const string RequireAdminRole = nameof(RequireAdminRole);
        public const string RequireClientRoleCompleted = nameof(RequireClientRoleCompleted);
        public const string RequireClientRole = nameof(RequireClientRole);
    }
}
