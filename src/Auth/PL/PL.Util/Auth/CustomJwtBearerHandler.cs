﻿using BL.Interfaces.Common;
using Common.Util;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Microsoft.Net.Http.Headers;
using System;
using System.Net;
using System.Text.Encodings.Web;
using System.Threading.Tasks;

namespace PL.Util.Auth
{
    public class CustomJwtBearerHandler : JwtBearerHandler
    {
        private readonly ICurrentUserService _currentUserService;

        public CustomJwtBearerHandler(IOptionsMonitor<JwtBearerOptions> options, ILoggerFactory logger, UrlEncoder encoder, ISystemClock clock, ICurrentUserService currentUserService) : base(options, logger, encoder, clock)
        {
            _currentUserService = currentUserService;
        }

        protected override async Task<AuthenticateResult> HandleAuthenticateAsync()
        {
            var baseAuthResult = await base.HandleAuthenticateAsync();
            return await HandleCustomAuthentication(baseAuthResult);
        }

        private async Task<AuthenticateResult> HandleCustomAuthentication(AuthenticateResult baseResult)
        {
            var jwtToken = GetJwtFromAuthHeader();
            var userInCache = await _currentUserService.InitUserInfoAsync(jwtToken);
            return userInCache
                ? baseResult
                : AuthenticateResult.Fail("Cache is empty, try to refresh the token");
        }

        private string GetJwtFromAuthHeader()
        {
            try
            {
                var authHeader = Context.Request.Headers[HeaderNames.Authorization];
                authHeader.ToArray();
                return authHeader.ToArray()[0].Split(' ')[1];
            }
            catch (Exception ex)
            {
                Logger.LogWarning(LoggerEvents.GetJwtFromAuthHeader, ex, $"Error trying get jwt from auth header: {Context.Request.Headers[HttpRequestHeader.Authorization.ToString()]}");
                return string.Empty;
            }
        }
    }
}
