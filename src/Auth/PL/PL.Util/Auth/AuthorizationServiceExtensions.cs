﻿using System;
using System.Collections.Generic;
using System.Text;
using Common.Util;
using Common.Util.Enums;
using Microsoft.Extensions.DependencyInjection;

namespace PL.Util.Auth
{
    public static class AuthorizationServiceExtensions
    {
        public static IServiceCollection AddCustomAuthorization(this IServiceCollection services)
        {
            services.AddAuthorization(options =>
            {
                options.AddPolicy(AuthorizationPolicyTypes.RequireAdminRole,
                    policy =>
                    {
                        policy.RequireClaim(Constants.Strings.ApplicationClaimTypes.Role, nameof(UserRoleType.Host));
                    });
                options.AddPolicy(AuthorizationPolicyTypes.RequireClientRoleCompleted,
                    policy =>
                    {
                        policy.RequireClaim(Constants.Strings.ApplicationClaimTypes.Role, (nameof(UserRoleType.Client)));
                        policy.RequireClaim(Constants.Strings.ApplicationClaimTypes.AccountState, (nameof(AccountStateType.Completed)));
                    });
                options.AddPolicy(AuthorizationPolicyTypes.AllowAll,
                    policy =>
                    {
                        policy.RequireAuthenticatedUser();
                    });
                options.AddPolicy(AuthorizationPolicyTypes.RequireClientRole,
                    policy =>
                    {
                        policy.RequireClaim(Constants.Strings.ApplicationClaimTypes.Role, (nameof(UserRoleType.Client)));
                    });
            });
            return services;
        }
    }
}
