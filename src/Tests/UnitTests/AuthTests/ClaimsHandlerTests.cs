﻿using Common.Util;
using Common.Util.Enums;
using DL.Entities;
using Microsoft.AspNetCore.Identity;
using Moq;
using NUnit.Framework;
using System.Linq;

namespace UnitTests.AuthTests
{
    public class ClaimsHandlerTests
    {
        [Test]
        public void GetClaimsIdentity_UserIsNull_ThrowArgumentNullException()
        {
            // arrange
            var userStore = new Mock<IUserStore<User>>();
            var userManager = new UserManager<User>(userStore.Object,);
            Assert.That(()=> _claimsHandler.GetClaimsIdentity(null), Throws.ArgumentNullException);
        }

        [Test]
        public void GetClaimsIdentity_WhenCalled_ReturnClaimsIdentity()
        {
            var result = _claimsHandler.GetClaimsIdentity(_user).GetAwaiter().GetResult();

            Assert.That(result, Is.Not.Null);
        }

        [Test]
        public void GetClaimsIdentity_WhenCalled_ReturnClaimsWithRoles()
        {
            var result = _claimsHandler.GetClaimsIdentity(_user).GetAwaiter().GetResult();

            var roleClaims =
                result.Claims
                    .Where(claim => claim.Type.Equals(Constants.Strings.ApplicationClaimTypes.Role))
                    .Select(claim=>claim.Value);
            
            Assert.That(roleClaims, Is.EquivalentTo(_roles));
        }

        [Test]
        public void GetClaimsIdentity_WhenCalled_ReturnClaimsWithUserStatus()
        {
            var result = _claimsHandler.GetClaimsIdentity(_user).GetAwaiter().GetResult();

            var accountStateClaim = result.Claims
                .First(claim => claim.Type.Equals(Constants.Strings.ApplicationClaimTypes.AccountState));
            
            Assert.That(accountStateClaim.Value.Equals((_user.UserStatus).ToString()));
        }

        private User NewUser(long id, string email, AccountStateType accountState)
        {
            return new User
            {
                Id = id,
                Email = email,
                UserStatus = accountState
            };
        }

    }
}
