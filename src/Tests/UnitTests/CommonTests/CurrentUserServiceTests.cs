﻿using BL.Common;
using BL.CommonModels;
using BL.Interfaces.Common;
using Microsoft.Extensions.Logging;
using Moq;
using Newtonsoft.Json;
using NUnit.Framework;
using StackExchange.Redis.Extensions.Core.Abstractions;

namespace UnitTests.CommonTests
{
    [TestFixture]
    public class CurrentUserServiceTests
    {
        private Mock<ILogger<CurrentUserService>> _logger;
        private Mock<IUserInfoService> _userInfoService;
        private UserInfo _userInfo;
        private CurrentUserService _currentUserService;


        [SetUp]
        public void SetUp()
        {
            _logger = new Mock<ILogger<CurrentUserService>>();
            _userInfoService = new Mock<IUserInfoService>();
            _userInfoService.Setup(service => service.Get(It.IsAny<string>()))
                .Returns(() => _userInfo);
            _userInfo = new UserInfo();
            _currentUserService = new CurrentUserService(_userInfoService.Object, _logger.Object);
        }

        [Test]
        [TestCase("")]
        [TestCase(" ")]
        [TestCase(null)]
        public void InitUserInfo_KeyIsNullOrWhiteSpace_ThrowArgumentNullException(string key)
        {
            Assert.That(()=>_currentUserService.InitUserInfoAsync(key), Throws.ArgumentNullException);
        }
        
        [Test]
        public void InitUserInfo_UserInfoServiceReturnsNull_ReturnFalse()
        {
            _userInfo = null;

            var result = _currentUserService.InitUserInfoAsync("a");

            Assert.That(result, Is.False);
        }

        [Test]
        public void InitUserInfo_WhenAct_ReturnTrue()
        {
            var result = _currentUserService.InitUserInfoAsync("a");

            Assert.That(result, Is.True);
        }


        [Test]
        public void InitUserInfo_WhenAct_UserInfoInitialized()
        {
            _currentUserService.InitUserInfoAsync("a");

            Assert.That(_currentUserService.UserInfo, Is.Not.Null);
        }
    }
}
