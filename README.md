# NetCore Pet project

## Contains:
*  EntityFramework based on IdentityDbContext
*  Separate migrations project
*  Microsoft Identity used for handling annoying things such as hashing a password, generating and validating some tokens, validating users, passwords, etc.
*  SMTP service for sending emails
*  RazorTemplates project for generating email templates
*  Base implementation for Hosted services which can be extended for any background tasks, and one extended implementation for sending emails
*  Redis cache service for storing user authorizations
*  Custom authentication middleware based on JWT tokens
*  Exception handler middleware
*  Swagger
* dockercompose

### Launch app using dockercompose.yml
Just set [SMTP](https://support.google.com/mail/answer/7126229?hl=ru) config in usersectrets 
> In Visual Studio, right-click the project PL.Web.Api in Solution Explorer, and select Manage User Secrets from the context menu.
```
{
    "Email":{
        "SmtpClient":{
            "UserName":"[your value]",
            "Port": "[yourValue]",
            "Password": "[your value]",
            "Host": "[your value]"
        }
    }
}
```

 